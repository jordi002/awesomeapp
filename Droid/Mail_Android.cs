﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AwesomeApp.Common;
using AwesomeApp.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(Mail_Android))]

namespace AwesomeApp.Droid
{
    public class Mail_Android : IMail
    {
        public void SendMail(string username, string password, string email)
        {
            {
                var fromAddress = new MailAddress("jgm17tfg@gmail.com", "Dr.Care");
                var toAddress = new MailAddress(email, username);
                const string fromPassword = "provestfg";
                const string subject = "PASSWORD-RECOVERY";
                string body = "<center> <h1>" + AppStrings.RecoveryBodyMailH1 + "</h1> <br> " +
                              AppStrings.RecoveryBodyMailString1 +
                              "<b>" + username + "</b> " + AppStrings.RecoveryBodyMailString2 + " <br> " +
                              "<b> " + password + "</b> </center>";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    message.IsBodyHtml = true;
                    smtp.Send(message);

                }
            }
        }
    }
}