﻿using Xamarin.Forms;

[assembly: ExportRenderer(typeof(Button), typeof(FlatButtonRenderer))]
/// <summary>
/// Created for enabling border radius and border color
/// </summary>
public class FlatButtonRenderer : Xamarin.Forms.Platform.Android.ButtonRenderer { }