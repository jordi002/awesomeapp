﻿using System;

using Xamarin.Forms;
using AwesomeApp;
using AwesomeApp.Droid;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(BorderlessEntry), typeof(BorderlessEntryRenderer))]
namespace AwesomeApp.Droid
{
    public class BorderlessEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                //Control.Background = null;
            }
        }
    }
}

