﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AwesomeApp.Common;


namespace AwesomeApp.Models
{
    class HAlimentacioObject : ObservableBase
    {
        long _id;
        public long id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        String _pregunta;
        public string pregunta
        {
            get { return _pregunta; }
            set { SetProperty(ref _pregunta, value); }
        }

        String _resposta;
        public string resposta
        {
            get { return _resposta; }
            set { SetProperty(ref _resposta, value); }
        }

        UserAccount _user_id;
        public UserAccount user_id
        {
            get { return _user_id; }
            set { SetProperty(ref _user_id, value); }
        }
    }
}
