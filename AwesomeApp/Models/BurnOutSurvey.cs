﻿using System;
using System.Collections.Generic;
using AwesomeApp.Common;
using Xamarin.Forms;


namespace AwesomeApp.Models
{
    /*
     * FROM MASLACH TO COPENHAGEN INVERTORY MIGRATION ANNOTATIONS 
     * 
     * About Copenhagen inventory: The psychometric qualities of the test make it a good diagnostic and preventive tool.
     * 
     * Maslach's MBI differencies: Unlike the Maslach Burnout Questionnaire (MBI), the CBI is royalty-free. 
     * It has been designed and validated in the context of research programmes involving large samples.
     * 
     * The new test analyses 3 different dimensions of burnout:
     *  Dimension 1 -> Personal burnout
     *  Dimension 2 -> Professional burnout
     *  Dimension 3 -> Relational burnout
     *  
     *  
     */
    public class BurnOutSurvey : ObservableBase
    {
        public BurnOutSurvey()
        {
            CBIquestions = new List<String>()
            {
                AppStrings.CBIDimension1Q1,
                AppStrings.CBIDimension1Q2,
                AppStrings.CBIDimension1Q3,
                AppStrings.CBIDimension1Q4,
                AppStrings.CBIDimension1Q5,
                AppStrings.CBIDimension1Q6,
                AppStrings.CBIDimension2Q1,
                AppStrings.CBIDimension2Q2,
                AppStrings.CBIDimension2Q3,
                AppStrings.CBIDimension2Q4,
                AppStrings.CBIDimension2Q5,
                AppStrings.CBIDimension2Q6,
                AppStrings.CBIDimension2Q7,
                AppStrings.CBIDimension3Q1,
                AppStrings.CBIDimension3Q2,
                AppStrings.CBIDimension3Q3,
                AppStrings.CBIDimension3Q4,
                AppStrings.CBIDimension3Q5,
                AppStrings.CBIDimension3Q6,
            };

            Utils.Shuffle(CBIquestions);

            OptionsPack1 = new List<Options>() {
                new Options(AppStrings.CBIP1Answer1, "Next.png"),
                new Options(AppStrings.CBIP1Answer2, "Next.png"),
                new Options(AppStrings.CBIP1Answer3, "Next.png"),
                new Options(AppStrings.CBIP1Answer4, "Next.png"),
                new Options(AppStrings.CBIP1Answer5, "Next.png"),
            };

            OptionsPack2 = new List<Options>() {
                new Options(AppStrings.CBIP2Answer1, "Next.png"),
                new Options(AppStrings.CBIP2Answer2, "Next.png"),
                new Options(AppStrings.CBIP2Answer3, "Next.png"),
                new Options(AppStrings.CBIP2Answer4, "Next.png"),
                new Options(AppStrings.CBIP2Answer5, "Next.png"),
            };

            QuestionDimensions = new Dictionary<String, String>();
            QuestionDimensions.Add(AppStrings.CBIDimension1Q1, "1");
            QuestionDimensions.Add(AppStrings.CBIDimension1Q2, "1");
            QuestionDimensions.Add(AppStrings.CBIDimension1Q3, "1");
            QuestionDimensions.Add(AppStrings.CBIDimension1Q4, "1");
            QuestionDimensions.Add(AppStrings.CBIDimension1Q5, "1");
            QuestionDimensions.Add(AppStrings.CBIDimension1Q6, "1");
            QuestionDimensions.Add(AppStrings.CBIDimension2Q1, "2");
            QuestionDimensions.Add(AppStrings.CBIDimension2Q2, "2");
            QuestionDimensions.Add(AppStrings.CBIDimension2Q3, "2");
            QuestionDimensions.Add(AppStrings.CBIDimension2Q4, "2");
            QuestionDimensions.Add(AppStrings.CBIDimension2Q5, "2");
            QuestionDimensions.Add(AppStrings.CBIDimension2Q6, "2");
            QuestionDimensions.Add(AppStrings.CBIDimension2Q7, "2");
            QuestionDimensions.Add(AppStrings.CBIDimension3Q1, "3");
            QuestionDimensions.Add(AppStrings.CBIDimension3Q2, "3");
            QuestionDimensions.Add(AppStrings.CBIDimension3Q3, "3");
            QuestionDimensions.Add(AppStrings.CBIDimension3Q4, "3");
            QuestionDimensions.Add(AppStrings.CBIDimension3Q5, "3");
            QuestionDimensions.Add(AppStrings.CBIDimension3Q6, "3");

            QuestionOptions = new Dictionary<String, String>();
            QuestionOptions.Add(AppStrings.CBIDimension1Q1, "OptionsPack1");
            QuestionOptions.Add(AppStrings.CBIDimension1Q2, "OptionsPack1");
            QuestionOptions.Add(AppStrings.CBIDimension1Q3, "OptionsPack1");
            QuestionOptions.Add(AppStrings.CBIDimension1Q4, "OptionsPack1");
            QuestionOptions.Add(AppStrings.CBIDimension1Q5, "OptionsPack1");
            QuestionOptions.Add(AppStrings.CBIDimension1Q6, "OptionsPack1");
            QuestionOptions.Add(AppStrings.CBIDimension2Q1, "OptionsPack2");
            QuestionOptions.Add(AppStrings.CBIDimension2Q2, "OptionsPack2");
            QuestionOptions.Add(AppStrings.CBIDimension2Q3, "OptionsPack2");
            QuestionOptions.Add(AppStrings.CBIDimension2Q4, "OptionsPack1");
            QuestionOptions.Add(AppStrings.CBIDimension2Q5, "OptionsPack1");
            QuestionOptions.Add(AppStrings.CBIDimension2Q6, "OptionsPack1");
            QuestionOptions.Add(AppStrings.CBIDimension2Q7, "OptionsPack1");
            QuestionOptions.Add(AppStrings.CBIDimension3Q1, "OptionsPack2");
            QuestionOptions.Add(AppStrings.CBIDimension3Q2, "OptionsPack2");
            QuestionOptions.Add(AppStrings.CBIDimension3Q3, "OptionsPack2");
            QuestionOptions.Add(AppStrings.CBIDimension3Q4, "OptionsPack2");
            QuestionOptions.Add(AppStrings.CBIDimension3Q5, "OptionsPack1");
            QuestionOptions.Add(AppStrings.CBIDimension3Q6, "OptionsPack1");
        }

        /**
         * Llista amb totes les preguntes del CBI, la qual es retornarà barrejada al fer el seu GET - DONE
         * 
         * Diccionari per guardar correspondència entre pregunta i dimensió del CBI                 - DONE
         * 
         * Diccionari per guardar correspondència entre pregunta i pack de respostes                - DONE
         * 
         * 
         * */

        List<String> _cbiQuestions;
        public List<String> CBIquestions
        {
            get { return _cbiQuestions; }
            set { SetProperty(ref _cbiQuestions, value); }
        }

        List<Options> _optionsPack1;
        public List<Options> OptionsPack1
        {
            get { return _optionsPack1; }
            set { SetProperty(ref _optionsPack1, value); }
        }

        List<Options> _optionsPack2;
        public List<Options> OptionsPack2
        {
            get { return _optionsPack2; }
            set { SetProperty(ref _optionsPack2, value); }
        }

        Dictionary<String,String> _questionDimensions;
        public Dictionary<String, String> QuestionDimensions
        {
            get { return _questionDimensions; }
            set { SetProperty(ref _questionDimensions, value); }
        }

        Dictionary<String, String> _questionOptions;
        public Dictionary<String, String> QuestionOptions
        {
            get { return _questionOptions; }
            set { SetProperty(ref _questionOptions, value); }
        }

        long _id;
        public long id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        DateTime _date;
        public DateTime date
        {
            get { return _date; }
            set { SetProperty(ref _date, value); }
        }

        #region Total Puntution from a BurnOut Survey
       /* int _totalpuntuation;
        public int TotalPuntuation
        {
            get { return _totalpuntuation; }
            set { SetProperty(ref _totalpuntuation, value); }
        }*/
        #endregion

        #region BurnOut Subscales Puntuations

        int _emotionalExhaust;
        public int emotionalExhaust
        {
            get { return _emotionalExhaust; }
            set { SetProperty(ref _emotionalExhaust, value); }
        }

        int _despersonalization;
        public int despersonalization
        {
            get { return _despersonalization; }
            set { SetProperty(ref _despersonalization, value); }
        }

        int _personalFullfillment;
        public int personalFullfillment
        {
            get { return _personalFullfillment; }
            set { SetProperty(ref _personalFullfillment, value); }
        }

        #endregion

        #region BurnOut SubScales Valorations

        string _emotionalExhaustValoration;
        public string emotionalExhaustValoration
        {
            get { return _emotionalExhaustValoration; }
            set { SetProperty(ref _emotionalExhaustValoration, value); }
        }
        string _despersonalitzationValoration;
        public string despersonalizationValoration
        {
            get { return _despersonalitzationValoration; }
            set { SetProperty(ref _despersonalitzationValoration, value); }
        }
        string _personalFullfillmentValoration;
        public string personalFullfillmentValoration
        {
            get { return _personalFullfillmentValoration; }
            set { SetProperty(ref _personalFullfillmentValoration, value); }
        }

        #endregion
        string _icon;
        public string Icon
        {
            get { return _icon; }
            set { SetProperty(ref _icon, value); }
        }

        UserAccount _user_id;
        public UserAccount user_id
        {
            get { return _user_id; }
            set { SetProperty(ref _user_id, value); }
        }
    }
}
    


