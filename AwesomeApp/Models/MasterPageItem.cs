﻿using System;
using AwesomeApp.Common;

namespace AwesomeApp
{
    public class MasterPageItem : ObservableBase
    {
        String _base;
        public string @Base
        {
            get { return _base; }
            set { SetProperty(ref _base, value); }
        }

        String _title;
        public string Title
        {
            get { return _title; }
            set{SetProperty( ref _title, value);}
        }

        String _iconsource;
        public string IconSource
        {
            get { return _iconsource; }
            set { SetProperty(ref _iconsource, value); }
        }

        Type _targettype;
        public Type TargetType
        {
            get { return _targettype; }
            set{SetProperty( ref _targettype, value);}
        }
    }
}