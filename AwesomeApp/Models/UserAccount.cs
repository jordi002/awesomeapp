﻿using System;
using System.Collections.Generic;
using AwesomeApp.Common;
using AwesomeApp.Models;

namespace AwesomeApp.Models
{
    public class UserAccount : ObservableBase
    {
        long _id;
        public long id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        #region AccountData
        String _username;
        public string mUsername
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        String _password;
        public string password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        String _email;
        public string mEmail
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }
        #endregion

        #region PersonalData
        String _cognom1;
        public string cognom1
        {
            get { return _cognom1; }
            set { SetProperty(ref _cognom1, value); }
        }

        String _cognom2;
        public string cognom2
        {
            get { return _cognom2; }
            set { SetProperty(ref _cognom2, value); }
        }

        String _nom;
        public string nom
        {
            get { return _nom; }
            set { SetProperty(ref _nom, value); }
        }
        String _edat;
        public String edat
        {
            get { return _edat; }
            set { SetProperty(ref _edat, value); }
        }

        String _professio;
        public string professio
        {
            get { return _professio; }
            set { SetProperty(ref _professio, value); }
        }

        String _anysExp;
        public String anysExp
        {
            get { return _anysExp; }
            set { SetProperty(ref _anysExp, value); }
        }

      String _genere;
        public string genere
        {
            get { return _genere; }
            set { SetProperty(ref _genere, value); }
        }
        #endregion

        #region Habits
        /*Dictionary<string, string> _halimentacio;
        public Dictionary<string, string> HAlimentacio
        {
            get { return _halimentacio; }
            set { SetProperty(ref _halimentacio, value); }
        }

        Dictionary<string, string> _hactivitatfisica;
        public Dictionary<string, string> HActivitatFisica
        {
            get { return _hactivitatfisica; }
            set { SetProperty(ref _hactivitatfisica, value); }
        }

        Dictionary<string, string> _htoxics;
        public Dictionary<string, string> HToxics
        {
            get { return _htoxics; }
            set { SetProperty(ref _htoxics, value); }
        }*/

        List<HAlimentacioObject> _halimentacio;
        List<HAlimentacioObject> hAlimentacio
        {
            get { return _halimentacio; }
            set { SetProperty(ref _halimentacio, value); }
        }
        List<HAlimentacioObject> _hactivitatfisica;
        List<HAlimentacioObject> hActivitatFisica
        {
            get { return _halimentacio; }
            set { SetProperty(ref _halimentacio, value); }
        }
        List<HAlimentacioObject> _htoxics;
        List<HAlimentacioObject> hToxics
        {
            get { return _halimentacio; }
            set { SetProperty(ref _halimentacio, value); }
        }
        #endregion

        #region BurnOut Stats
        List<BurnOutSurvey> _burnoutstats = new List<BurnOutSurvey>();
        public List<BurnOutSurvey> BurnOutStats
        {
            get { return _burnoutstats; }
            set { SetProperty(ref _burnoutstats, value); }
        }
        #endregion

        #region Goldberg Stats
        List<GoldbergSurvey> _goldbergstats = new List<GoldbergSurvey>();
        public List<GoldbergSurvey> GoldbergStats
        {
            get { return _goldbergstats; }
            set { SetProperty(ref _goldbergstats, value); }
        }
        #endregion

        Boolean _firstTimeLoggingIn = true;
        public Boolean FirstTimeLoggingIn
        {
            get { return _firstTimeLoggingIn; }
            set { SetProperty(ref _firstTimeLoggingIn, value); }
        }
    }
}