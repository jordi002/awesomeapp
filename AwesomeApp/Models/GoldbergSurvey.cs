﻿using AwesomeApp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwesomeApp.Models
{
    public class GoldbergSurvey : ObservableBase
    {
        long _id;
        public long id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        #region Data
        DateTime _date;
        public DateTime date
        {
            get { return _date; }
            set { SetProperty(ref _date, value); }
        }
        #endregion

        /*#region Questions & Answers
        Dictionary<string, string> _goldbergqa;
        public Dictionary<string, string> GoldbergQA
        {
            get { return _goldbergqa; }
            set { SetProperty(ref _goldbergqa, value); }
        }
        #endregion*/

        #region Malestar Psicologic
        Boolean _malestarpsicologic;
        public Boolean malestarPsicologic
        {
            get { return _malestarpsicologic; }
            set { SetProperty(ref _malestarpsicologic, value); }
        }
        #endregion

        UserAccount _user_id;
        public UserAccount user_id
        {
            get { return _user_id; }
            set { SetProperty(ref _user_id, value); }
        }
    }
}
