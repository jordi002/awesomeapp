﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwesomeApp.Common
{
    public interface IMail
    {
        void SendMail(string username, string password, string email);

    }

}
