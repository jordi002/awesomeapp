﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace AwesomeApp.Common
{
    public interface ILocalize
    {
        CultureInfo GetCurrentCultureInfo();
        void SetLocale(CultureInfo ci);
    }
}


