﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AwesomeApp.Common;
using AwesomeApp.Models;
using AwesomeApp.ViewModels;
using ModernHttpClient;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace AwesomeApp.ViewModels
{
    public class HAlimentacioViewModel : ObservableBase
    {
        public HAlimentacioViewModel()
        {
            
            OptionsPack1 = new List<string>() {
                "   0",
                "   1",
                "   2",
                "   3",
                "   4",
                "   5",
                AppStrings.HAlimentacio_PickerOption7
            };
            
            OptionsPack2 = new List<Options>() {
                new Options(AppStrings.HAlimentacio_Answer1,"Next.png"),
                new Options(AppStrings.HAlimentacio_Answer2,"Next.png"),
                new Options(AppStrings.HAlimentacio_Answer3,"Next.png"),
                new Options(AppStrings.HAlimentacio_Answer4,"Next.png")
            };
            

            alimentacio_survey.preguntes = new List<string>()
            {
                AppStrings.HAlimentacio_Question4,
                AppStrings.HAlimentacio_Question5,
                AppStrings.HAlimentacio_Question6,
                AppStrings.HAlimentacio_Question7,
                AppStrings.HAlimentacio_Question8,

            };



            Values = new List<string>() { "1", "2", "3", "4", "5" };

            alimentacio_survey.index = 0;
            Pregunta = alimentacio_survey.preguntes.ElementAt(alimentacio_survey.index);

        }
        public static Dictionary<string, string> _halimentacio = new Dictionary<string, string>();
        public Dictionary<string, string> HAlimentacio
        {
            get { return _halimentacio; }
            set { SetProperty(ref _halimentacio, value); }
        }

        public double _progress;
        public double Progress
        {
            get { return _progress; }
            set { SetProperty(ref _progress, value); }
        }
        public string _pregunta;
        public string Pregunta
        {
            get { return _pregunta; }
            set { SetProperty(ref _pregunta, value); }
        }


        public List<string> _optionsPack1;
        public List<string> OptionsPack1
        {
            get { return _optionsPack1; }
            set { SetProperty(ref _optionsPack1, value); }
        }
        public  List<Options> _optionsPack2;
        public List<Options> OptionsPack2
        {
            get { return _optionsPack2; }
            set { SetProperty(ref _optionsPack2, value); }
        }
       
        public List<string> _values;
        public List<string> Values
        {
            get { return _values; }
            set { SetProperty(ref _values, value); }
        }
        public void SaveSliderAnswers(int val1, int val2)
        {
            HAlimentacio.Clear();
            HAlimentacio[AppStrings.HAlimentacio_Question1] = val1.ToString();
            HAlimentacio[AppStrings.HAlimentacio_Question2] = val2.ToString();

        }

        public void SavePickerAnswer(string answer)
        {
            if(HAlimentacio.Count >= 3)
            {
                HAlimentacio[AppStrings.HAlimentacio_Question3] = answer;            
            }
            else
            {
                HAlimentacio[AppStrings.HAlimentacio_Question3] = answer;
            }

        }
        public void Handle_Questions_Answers(string answer)
        {
            HAlimentacio[Pregunta] = answer;

            if (alimentacio_survey.index < 4)
            {
                alimentacio_survey.index++;

                Pregunta = alimentacio_survey.preguntes.ElementAt(alimentacio_survey.index);
            }
            alimentacio_survey.progress +=0.125;
            Progress = alimentacio_survey.progress;
        }

       /* public async Task SaveHAlimentacioAsync()
        {
            String currentUser;
            currentUser = await Database.Instance.GetObject<String>(Tables.CURRENT_USER);

            List<UserAccount> users;
            users = await Database.Instance.GetObject<List<UserAccount>>(Tables.REGISTERED_USERS);

            if (users != null)
            {
                UserAccount result = users.Find(x => x.mUsername == currentUser);
                users.ElementAt(users.IndexOf(users.Find(x => x.mUsername == currentUser))).HAlimentacio = HAlimentacio;
                Database.Instance.InsertObject(Tables.REGISTERED_USERS, users);
            }
        }*/
        public void Save()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/halimentacio", string.Empty));
            HttpResponseMessage response = null;

            foreach (KeyValuePair<string, string> entry in HAlimentacio)
            {
                var ha = new HAlimentacioObject() { pregunta = entry.Key, resposta=entry.Value , user_id = new UserAccount() { id = GetUserId() } };
                var json = JsonConvert.SerializeObject(ha);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                response = Task.Run(() => client.PostAsync(uri, content)).Result;
            }

        }

        long GetUserId()
        {
            //UserAccount currentUser;
            //currentUser = Task.Run(() => Database.Instance.GetObject<UserAccount>(Tables.CURRENT_USER)).Result;

            return CurrentUser.currentUser.id;
        }
    }

    public static class alimentacio_survey
    {
        public static List<string> preguntes = new List<string>();
        public static int index;
        public static double progress;
    }

}