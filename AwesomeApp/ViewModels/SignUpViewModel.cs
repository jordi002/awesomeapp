﻿using System;
using Xamarin.Forms;
using AwesomeApp.Common;
using Akavache;
using System.Threading.Tasks;
using System.Collections.Generic;
using AwesomeApp.Models;
using System.Globalization;

namespace AwesomeApp.ViewModels
{
    public class SignUpViewModel : ObservableBase
    {
        public SignUpViewModel(){
            
        List<string> especialitats = new List<string>
            {
                AppStrings.SignUpProfessionsPickerOp1,
                AppStrings.SignUpProfessionsPickerOp2,
                AppStrings.SignUpProfessionsPickerOp3,
                AppStrings.SignUpProfessionsPickerOp4,
                AppStrings.SignUpProfessionsPickerOp5,
                AppStrings.SignUpProfessionsPickerOp6,
                AppStrings.SignUpProfessionsPickerOp7,
                AppStrings.SignUpProfessionsPickerOp8,
            };

            Especialitats = especialitats;

            List<string> generes = new List<string>
            {
                AppStrings.SignUpGenderPickerOp1,
                AppStrings.SignUpGenderPickerOp2
            };

            GenereOptions = generes;

            ButtonEnabled = false;
        }



    public List<string> _especialitats;
    public List<string> Especialitats
    {
            get { return _especialitats; }
            set { SetProperty(ref _especialitats, value); }
    }
        public List<string> _genereOptions;
        public List<string> GenereOptions
        {
            get { return _genereOptions; }
            set { SetProperty(ref _genereOptions, value); }
        }
        public static UserAccount user = new UserAccount();

        String _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        String _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        String _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        String _cognom1;
        public string Cognom1
        {
            get { return _cognom1; }
            set { SetProperty(ref _cognom1, value); }
        }

        String _cognom2;
        public string Cognom2
        {
            get { return _cognom2; }
            set { SetProperty(ref _cognom2, value); }
        }

        String _nom;
        public string Nom
        {
            get { return _nom; }
            set { SetProperty(ref _nom, value); }
        }
        string _edat;
        public string Edat
        {
            get { return _edat; }
            set { SetProperty(ref _edat, value); }
        }

        String _professio;
        public string Professio
        {
            get { return _professio; }
            set { SetProperty(ref _professio, value); }
        }

        string _anysExp;
        public string AnysExp
        {
            get { return _anysExp; }
            set { SetProperty(ref _anysExp, value); }
        }

        String _genere;
        public string Genere
        {
            get { return _genere; }
            set { SetProperty(ref _genere, value); }
        }

        bool _buttonEnabled;
        public bool ButtonEnabled
        {
            get { return _buttonEnabled; }
            set { SetProperty(ref _buttonEnabled, value); }
        }

        public bool DataIsNotEmptyP1 { get => !string.IsNullOrEmpty(user.mUsername) && !string.IsNullOrEmpty(user.password) && !string.IsNullOrEmpty(user.mEmail); }
        public bool DataIsNotEmptyP2 { get => !string.IsNullOrEmpty(user.cognom1) && !string.IsNullOrEmpty(user.cognom2) && !string.IsNullOrEmpty(user.nom) && !string.IsNullOrEmpty(user.professio) && !string.IsNullOrEmpty(user.edat) && !string.IsNullOrEmpty(user.anysExp) && !string.IsNullOrEmpty(user.genere); }

        /*public async Task<bool> UsernameAlreadyExists()
        {
            List<UserAccount> users;
            users = await Database.Instance.GetObject<List<UserAccount>>(Tables.REGISTERED_USERS);

            if (users != null)
            {

            
                    foreach (var u in users)
                    {
                        if (u.mUsername.Equals(user.mUsername))
                        {
                            return true;
                        }
                    }
            }
            return false;

        }*/
        public  Boolean PasswordFormatIsOk()
        {
            int capsCounter = 0;
            int numbersCounter = 0;

            for (int i = 0; i < user.password.Length; i++){
                if(System.Char.IsNumber(user.password, i)){
                    numbersCounter++;
                }
                if(Char.IsLetter(Password[i])){
                    if(user.password[i].Equals(user.password.ToUpper()[i])){
                        capsCounter++;
                    }
				}
            }
            return capsCounter>0 && numbersCounter>0 && user.password.Length>=3 && user.password.Length <= 16;

        }
        public  Boolean EmailFormatIsOk()
        {
            if(user.mEmail.Contains("@")){
                
                var part1 = user.mEmail.Split('@')[0];
                var part2 = user.mEmail.Split('@')[1];
				
                if(part2.Contains(".")){
					var part3 = part2.Split('.');
					return part1.Length >= 4 && part2.Length>=3 && part3[1].Length>=3;
                }

            }
                return false;

        }
        public Boolean AgeFormatIsOk()
        {
            int n;
            bool isInt = int.TryParse(user.edat, out n);
            if(isInt){
                n = int.Parse(user.edat);
				return n > 0 && n < 105 && isInt;
            }
            return false;
        }
        public Boolean ExpFormatIsOk()
        {
            int n;
            bool isInt = int.TryParse(user.anysExp, out n);
            if (isInt)
            {
                n = int.Parse(user.anysExp);
                return n >= 0 && n < 50 && isInt;
            }
            return false;           
        }
    }
}