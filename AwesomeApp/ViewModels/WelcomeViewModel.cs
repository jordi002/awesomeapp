﻿using AwesomeApp.Common;
using AwesomeApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwesomeApp.ViewModels
{
    class WelcomeViewModel : ObservableBase
    {
        public WelcomeViewModel()
        {
            GetUserName();
            

        }

        public String _welcoming;
        public String Welcoming
        {
            get { return _welcoming; }
            set { SetProperty(ref _welcoming, value); }
        }

        private void GetUserName()
        {
            //UserAccount currentUser;
            //currentUser = await Database.Instance.GetObject<UserAccount>(Tables.CURRENT_USER);

            /* List<UserAccount> users;
             users = await Database.Instance.GetObject<List<UserAccount>>(Tables.REGISTERED_USERS);

             if (users != null)
             {
                 Welcoming = AppStrings.WelcomePage1Title + users.ElementAt(users.IndexOf(users.Find(x => x.mUsername == currentUser))).nom + "!";               
             }*/

            Welcoming = AppStrings.WelcomePage1Title + CurrentUser.currentUser.nom + "!";
            
        }
    }
}
