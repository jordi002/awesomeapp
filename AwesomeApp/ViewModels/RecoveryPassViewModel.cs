﻿using System;
using System.Collections.Generic;
using AwesomeApp.Models;
using AwesomeApp.ViewModels;
using System.Linq;

using AwesomeApp.Common;
using Xamarin.Forms;
using Akavache;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;
using System.Net.Http;
using ModernHttpClient;
using Newtonsoft.Json;
using System.Text;

namespace AwesomeApp.ViewModels
{
    public class RecoveryPassViewModel : ObservableBase
    {
        public RecoveryPassViewModel()
        {

        }
            

        public static string Username { get; set; }
        public string Password1 { get; set; }
        public string Password2 { get; set; }
        public string Email { get; set; }
        

        public bool DataIsNotEmpty => !string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Email);
        public bool DataIsNotEmpty2 => !string.IsNullOrEmpty(Password1) && !string.IsNullOrEmpty(Password2);

        /* public async Task<bool> CheckRecFormAsync()
         {
             List<UserAccount> users;
             users = await Database.Instance.GetObject<List<UserAccount>>(Tables.REGISTERED_USERS);

             foreach (var u in users)
             {
                 if (u.mEmail.Equals(Email) && u.mUsername.Equals(Username))
                 {
                     string = u.Email;
                     string pass = u.Password;


                     Email = u.mEmail;
                     Password = u.password;
                     return true;
                 }
             }

             return false;

         }*/

        //public void SendEmailAsync() => DependencyService.Get<IMail>().SendMail(Username, Password, Email);

        public Boolean CheckRecForm()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/user" +"/newPasswordRequest", string.Empty));
            HttpResponseMessage response = null;

            UserAccount User = new UserAccount() { mUsername= Username, mEmail=Email};
            var json = JsonConvert.SerializeObject(User);

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            response = Task.Run(() => client.PostAsync(uri, content)).Result;

            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }
        public Boolean PasswordFormatIsOk()
        {
            int capsCounter = 0;
            int numbersCounter = 0;

                for (int i = 0; i < Password1.Length; i++)
                {
                    if (System.Char.IsNumber(Password1, i))
                    {
                        numbersCounter++;
                    }
                    if (Char.IsLetter(Password1[i]))
                    {
                        if (Password1[i].Equals(Password1.ToUpper()[i]))
                        {
                            capsCounter++;
                        }
                    }
                }

            return capsCounter > 0 && numbersCounter > 0 && Password1.Length >= 3 && Password1.Length <= 16;

        }

        public void SaveNewPassword()
        {
            long id = GetUserId();

            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/user/" + id+ "/modify", string.Empty));
            HttpResponseMessage response = null;

            UserAccount User = new UserAccount() { password=Password1};
            var json = JsonConvert.SerializeObject(User);

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            response = Task.Run(() => client.PutAsync(uri, content)).Result;
        }
        long GetUserId()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/user", string.Empty));
            HttpResponseMessage response = null;
            UserAccount User = new UserAccount() { mUsername = Username };
            var json = JsonConvert.SerializeObject(User);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            response = Task.Run(() => client.PostAsync(uri, content)).Result;

            if (response.IsSuccessStatusCode)
            {
                var c = response.Content.ReadAsStringAsync().Result;
                var r = JsonConvert.DeserializeObject<UserAccount>(c);
                return r.id;
            }
            return 0;
        }
    }
    }


