﻿using System;
using AwesomeApp.Common;
using Xamarin.Forms;
using AwesomeApp.Models;
using System.Collections.Generic;
using System.Linq;
using AwesomeApp.Views;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Net.Http;
using ModernHttpClient;
using Newtonsoft.Json;
using System.Text;

namespace AwesomeApp.ViewModels
{
    public class BurnOutViewModel : ObservableBase
    {
        public BurnOutViewModel()
        {
            BoPersonal = 0;
            BoProfessional = 0;
            BoRelational = 0;

            survey.preguntes = BOS.CBIquestions;

            survey.index = 0;

            Pregunta = survey.preguntes.ElementAt(survey.index);

            if ("OptionsPack1".Equals(BOS.QuestionOptions[Pregunta]))
            {
                Options = BOS.OptionsPack1;
            }
            else
            {
                Options = BOS.OptionsPack2;
            }

            survey.progress = 0.0;
            survey.progress += 0.053;
            Progress = survey.progress;

        }

        public BurnOutViewModel(int boPersonal, int boProfessional, int boRelational)
        {
            BoPersonal = boPersonal;
            BoProfessional = boProfessional;
            BoRelational = boRelational;

            CalculateResults();
        }

        public static BurnOutSurvey _bos = new BurnOutSurvey();
        public BurnOutSurvey BOS
        {
            get { return _bos; }
            set { SetProperty(ref _bos, value); }
        }

        Dictionary<string, string> _burnout = new Dictionary<string, string>();
        public Dictionary<string, string> BurnOut
        {
            get { return _burnout; }
            set { SetProperty(ref _burnout, value); }
        }

        public double _progress;
        public double Progress
        {
            get { return _progress; }
            set{SetProperty(ref _progress, value);}
        }
        public string _pregunta;
        public string Pregunta
        {
            get { return _pregunta; }
            set{SetProperty(ref _pregunta, value);}
        }

        public List<Options> _options;
        public List<Options> Options
        {
            get { return _options; }
            set { SetProperty(ref _options, value); }
        }
        public int _boPersonal;
        public int BoPersonal
        {
            get { return _boPersonal; }
            set { SetProperty(ref _boPersonal, value); }
        }

        public int _boProfessional;
        public int BoProfessional
        {
            get { return _boProfessional; }
            set { SetProperty(ref _boProfessional, value); }
        }

        public int _boRelational;
        public int BoRelational
        {
            get { return _boRelational; }
            set { SetProperty(ref _boRelational, value); }
        }
        string _boPersonalValoration;
        public string BoPersonalValoration
        {
            get { return _boPersonalValoration; }
            set { SetProperty(ref _boPersonalValoration, value); }
        }
        string _boProfessionalValoration;
        public string BoProfessionalValoration
        {
            get { return _boProfessionalValoration; }
            set { SetProperty(ref _boProfessionalValoration, value); }
        }
        string _boRelationalValoration;
        public string BoRelationalValoration
        {
            get { return _boRelationalValoration; }
            set { SetProperty(ref _boRelationalValoration, value); }
        }

        Boolean _isPremium;
        public Boolean IsPremium
        {
            get { return _isPremium; }
            set { SetProperty(ref _isPremium, value); }
        }

        public void CalculateResults()
        {
            /*
             * CBI adapted
            BoPersonal = CalculateBoPersonal();
            BoProfessional = CalculateBoProfessional();
            BoRelational = CalculateBoRelational();
            */
            BoPersonal = BoPersonal / 6;
            BoProfessional = BoProfessional / 7;
            BoRelational = BoRelational / 6;

            CalculateBoPersonalValoration();
            CalculateBoProfessionalValoration();
            CalculateBoRelationalValoration();

            //survey.respostes.Clear();

            //SaveAsync();
            Save();
        }

        public int CalculateBoPersonal(){
            int res = survey.respostes.ElementAt(0) + survey.respostes.ElementAt(1) + survey.respostes.ElementAt(2)
                             + survey.respostes.ElementAt(5) + survey.respostes.ElementAt(7) + survey.respostes.ElementAt(12)
                             + survey.respostes.ElementAt(13) + survey.respostes.ElementAt(15) + survey.respostes.ElementAt(19);
            return res;
        }
        public int CalculateBoProfessional(){
            int res = survey.respostes.ElementAt(4) + survey.respostes.ElementAt(9) + survey.respostes.ElementAt(10)
                             + survey.respostes.ElementAt(14) + survey.respostes.ElementAt(21);
            return res;
        }
        public int CalculateBoRelational(){
            int res = survey.respostes.ElementAt(3) + survey.respostes.ElementAt(6) + survey.respostes.ElementAt(8)
                             + survey.respostes.ElementAt(11) + survey.respostes.ElementAt(16) + survey.respostes.ElementAt(17)
                             + survey.respostes.ElementAt(18) + survey.respostes.ElementAt(20);
            return res;
        }

        public void CalculateBoPersonalValoration()
        {
            if(BoPersonal < 50){
                BoPersonalValoration = AppStrings.BurnOutResultsValorationLow;
            }else if(BoPersonal >= 50 && BoPersonal < 75){
                BoPersonalValoration = AppStrings.BurnOutResultsValorationMedium;
            }else if(BoPersonal >= 75 && BoPersonal < 100){
                BoPersonalValoration = AppStrings.BurnOutResultsValorationHigh;
            }else if(BoPersonal == 100)
            {
                BoPersonalValoration = AppStrings.BurnOutResultsValorationSevere;
            }
    
        }
        public void CalculateBoProfessionalValoration()
        {
            if (BoProfessional < 50)
            {
                BoProfessionalValoration = AppStrings.BurnOutResultsValorationLow;
            }
            else if (BoProfessional >= 50 && BoProfessional < 75)
            {
                BoProfessionalValoration = AppStrings.BurnOutResultsValorationMedium;
            }
            else if (BoProfessional >= 75 && BoProfessional < 100)
            {
                BoProfessionalValoration = AppStrings.BurnOutResultsValorationHigh;
            }
            else if (BoProfessional == 100)
            {
                BoProfessionalValoration = AppStrings.BurnOutResultsValorationSevere;
            }
        }
        public void CalculateBoRelationalValoration()
        {
            if (BoRelational < 50)
            {
                BoRelationalValoration = AppStrings.BurnOutResultsValorationLow;
            }
            else if (BoRelational >= 50 && BoRelational < 75)
            {
                BoRelationalValoration = AppStrings.BurnOutResultsValorationMedium;
            }
            else if (BoRelational >= 75 && BoRelational < 100)
            {
                BoRelationalValoration = AppStrings.BurnOutResultsValorationHigh;
            }
            else if (BoRelational == 100)
            {
                BoRelationalValoration = AppStrings.BurnOutResultsValorationSevere;
            }
        }
        public int answerPuntuation(string answer)
        {
            if(answer.Equals(AppStrings.CBIP1Answer1) || answer.Equals(AppStrings.CBIP2Answer1))
            {
                if (!Pregunta.Equals(AppStrings.CBIDimension2Q7))
                {
                    return 100;
                }
                else
                {
                    return 0;
                }
            }else if (answer.Equals(AppStrings.CBIP1Answer2) || answer.Equals(AppStrings.CBIP2Answer2))
            {
                if (!Pregunta.Equals(AppStrings.CBIDimension2Q7))
                {
                    return 75;
                }
                else
                {
                    return 25;
                }
            }
            else if (answer.Equals(AppStrings.CBIP1Answer3) || answer.Equals(AppStrings.CBIP2Answer3))
            {
                if (!Pregunta.Equals(AppStrings.CBIDimension2Q7))
                {
                    return 50;
                }
                else
                {
                    return 50;
                }
            }
            else if (answer.Equals(AppStrings.CBIP1Answer4) || answer.Equals(AppStrings.CBIP2Answer4))
            {
                if (!Pregunta.Equals(AppStrings.CBIDimension2Q7))
                {
                    return 25;
                }
                else
                {
                    return 75;
                }
            }
            else if (answer.Equals(AppStrings.CBIP1Answer5) || answer.Equals(AppStrings.CBIP2Answer5))
            {
                if (!Pregunta.Equals(AppStrings.CBIDimension2Q7))
                {
                    return 0;
                }
                else
                {
                    return 100;
                }
            }

            return 0;
        }
        public void Handle_Questions_Answers(string answer){
            /* Deprecated since CBI adapt
            BurnOut[Pregunta] = answer;

            if(answer.Equals(AppStrings.CBIP1Answer1) || answer.Equals(AppStrings.CBIP2Answer1))
            {
                survey.respostes.Add(0);
            }
            if (answer.Equals(AppStrings.CBIP1Answer2) || answer.Equals(AppStrings.CBIP2Answer2))
            {
                survey.respostes.Add(1);
            }
            if (answer.Equals(AppStrings.CBIP1Answer3) || answer.Equals(AppStrings.CBIP2Answer3))
            {
                survey.respostes.Add(2);
            }
            if (answer.Equals(AppStrings.CBIP1Answer4) || answer.Equals(AppStrings.CBIP2Answer4))
            {
                survey.respostes.Add(3);
            }
            if (answer.Equals(AppStrings.CBIP1Answer5) || answer.Equals(AppStrings.CBIP2Answer5))
            {
                survey.respostes.Add(4);
            }
            */

            if ("1".Equals(BOS.QuestionDimensions[Pregunta]))
            {
                BoPersonal += answerPuntuation(answer);
            }else if ("2".Equals(BOS.QuestionDimensions[Pregunta]))
            {
                BoProfessional += answerPuntuation(answer);
            }
            else
            {
                BoRelational += answerPuntuation(answer);
            }

            if(survey.index < (survey.preguntes.Count -1))
            {
				survey.index++;
            
                Pregunta = survey.preguntes.ElementAt(survey.index);

                if ("OptionsPack1".Equals(BOS.QuestionOptions[Pregunta]))
                {
                    Options = BOS.OptionsPack1;
                }
                else
                {
                    Options = BOS.OptionsPack2;
                }

            }
            //0.05263157894736842105263157894737
            survey.progress += 0.053;
            Progress = survey.progress;

        }
        /* async void SaveAsync()
         {
             await SaveBurnOutAsync();
         }

         public async Task SaveBurnOutAsync()
         {
             String currentUser;
             currentUser = await Database.Instance.GetObject<String>(Tables.CURRENT_USER);

             List<UserAccount> users;
             users = await Database.Instance.GetObject<List<UserAccount>>(Tables.REGISTERED_USERS);


             if (users != null)
             {
                 UserAccount result = users.Find(x => x.mUsername == currentUser);

                 BOS.Date = DateTime.Now;

                 BOS.EmotionalExhaust = EmotionalExhaust;
                 BOS.Despersonalization = Despersonalization;
                 BOS.PersonalFullfillment = PersonalFullfillment;

                 BOS.EmotionalExhaustValoration = EmotionalExhaustValoration;
                 BOS.DespersonalizationValoration = DespersonalizationValoration;
                 BOS.PersonalFullfillmentValoration = PersonalFullfillmentValoration;

                 List<BurnOutSurvey> stats = users.ElementAt(users.IndexOf(users.Find(x => x.mUsername == currentUser))).BurnOutStats;
                 if(stats == null)
                 {
                     users.ElementAt(users.IndexOf(users.Find(x => x.mUsername == currentUser))).BurnOutStats = new List<BurnOutSurvey>
                     {
                         BOS
                     };
                 }
                 else
                 {
                     users.ElementAt(users.IndexOf(users.Find(x => x.mUsername == currentUser))).BurnOutStats.Add(BOS);
                 }
                 Database.Instance.InsertObject(Tables.REGISTERED_USERS, users);
             }
         }*/
        public void Save()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/registerMaslachRes", string.Empty));
            HttpResponseMessage response = null;

            BOS.date = DateTime.Now;
            /**
             * CBI adapted*/
            BOS.emotionalExhaust = BoPersonal;
            BOS.despersonalization = BoProfessional;
            BOS.personalFullfillment = BoRelational;

            BOS.emotionalExhaustValoration = BoPersonalValoration;
            BOS.despersonalizationValoration = BoProfessionalValoration;
            BOS.personalFullfillmentValoration = BoRelationalValoration;

            BOS.user_id = new UserAccount() { id = GetUserId() };

            var json = JsonConvert.SerializeObject(BOS);

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            response = Task.Run(() => client.PostAsync(uri, content)).Result;

        }

        long GetUserId()
        {
            //UserAccount currentUser;
            //currentUser = Task.Run(() => Database.Instance.GetObject<UserAccount>(Tables.CURRENT_USER)).Result;

            return CurrentUser.currentUser.id;
        }
    }

    public static class survey 
    {
        public static List<int> respostes = new List<int>();
        public static List<string> preguntes = new List<string>();
        public static int index;
        public static double progress;

        public static List<List<Options>> options = new List<List<Options>>();
    }

}


