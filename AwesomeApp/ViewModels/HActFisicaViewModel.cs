﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AwesomeApp.Common;
using AwesomeApp.Models;
using ModernHttpClient;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace AwesomeApp.ViewModels
{
    public class HActFisicaViewModel : ObservableBase
    {
        public HActFisicaViewModel()
        {
            var options1 = new List<string>(){
	            "0",
	            "1",
	            "2",
	            "3",
	            "4",
	            "5",
	            "6",
	            "7"
            };
            var options2 = new List<string>(){
	            AppStrings.HActivitatFisica_Q2Option1,
	            "45",
	            "60",
	            "90",
	            "120",
	            AppStrings.HActivitatFisica_Q2Option6,
            };

            actFisica_survey.exemples = new List<string>()
            {
                "",
                "",
                AppStrings.HactivitatFisica_Example1,
                "",
                AppStrings.HactivitatFisica_Example2,
                "",
            };
            actFisica_survey.labels = new List<string>()
            {
                AppStrings.HActivitatFisica_Label1,
                AppStrings.HActivitatFisica_Label2,
                AppStrings.HActivitatFisica_Label1,
                AppStrings.HActivitatFisica_Label2,
                AppStrings.HActivitatFisica_Label1,
                AppStrings.HActivitatFisica_Label2,
            };

            actFisica_survey.sources = new List<List<string>>() {
                options1,
                options2,
                options1,
                options2,
                options1,
                options2,
            };


            actFisica_survey.preguntes = new List<string>()
            {
                AppStrings.HActivitatFisica_Question1,
                AppStrings.HActivitatFisica_Question2,
                AppStrings.HActivitatFisica_Question3,
                AppStrings.HActivitatFisica_Question4,
                AppStrings.HActivitatFisica_Question5,
                AppStrings.HActivitatFisica_Question6,

            };

            actFisica_survey.index = 0;
            actFisica_survey.progress = 0.1666666666666667;

            Progress = actFisica_survey.progress;
            Pregunta = actFisica_survey.preguntes.ElementAt(actFisica_survey.index);
            Exemple = actFisica_survey.exemples.ElementAt(actFisica_survey.index);
            Source = actFisica_survey.sources.ElementAt(actFisica_survey.index);
            Label = actFisica_survey.labels.ElementAt(actFisica_survey.index);
        }
        public static Dictionary<string, string> _hactivitatfisica = new Dictionary<string, string>();
        public Dictionary<string, string> HActivitatFisica
        {
            get { return _hactivitatfisica; }
            set { SetProperty(ref _hactivitatfisica, value); }
        }

        public double _progress;
        public double Progress
        {
            get { return _progress; }
            set { SetProperty(ref _progress, value); }
        }
        public string _pregunta;
        public string Pregunta
        {
            get { return _pregunta; }
            set { SetProperty(ref _pregunta, value); }
        }
        public string _exemple;
        public string Exemple
        {
            get { return _exemple; }
            set { SetProperty(ref _exemple, value); }
        }
        public List<string> _source;
        public List<string> Source
        {
            get { return _source; }
            set { SetProperty(ref _source, value); }
        }
        public string _label;
        public string Label
        {
            get { return _label; }
            set { SetProperty(ref _label, value); }
        }

        public void Handle_Questions_Answers(string answer)
        {

            HActivitatFisica[Pregunta] = answer;

            if (actFisica_survey.index < 5)
            {
                if (Pregunta.Equals(actFisica_survey.preguntes[0]) && answer.Equals("0") ||
                   Pregunta.Equals(actFisica_survey.preguntes[2]) && answer.Equals("0") )               
                {
                    actFisica_survey.index += 2;
                    actFisica_survey.progress += 0.1666666666666667;
                }
                else if (Pregunta.Equals(actFisica_survey.preguntes[4]) && answer.Equals("0"))
                {

                }else{
                    actFisica_survey.index++;
                }

                Pregunta = actFisica_survey.preguntes.ElementAt(actFisica_survey.index);
                Exemple = actFisica_survey.exemples.ElementAt(actFisica_survey.index);
                Source = actFisica_survey.sources.ElementAt(actFisica_survey.index);
                Label = actFisica_survey.labels.ElementAt(actFisica_survey.index);
            }
            actFisica_survey.progress += 0.1666666666666667;
            Progress = actFisica_survey.progress;
        }

        /* public async Task SaveHActivitatFisicaAsync()
         {
             String currentUser;
             currentUser = await Database.Instance.GetObject<String>(Tables.CURRENT_USER);

             List<UserAccount> users;
             users = await Database.Instance.GetObject<List<UserAccount>>(Tables.REGISTERED_USERS);

             if (users != null)
             {
                 UserAccount result = users.Find(x => x.mUsername == currentUser);
                 users.ElementAt(users.IndexOf(users.Find(x => x.mUsername == currentUser))).HActivitatFisica = HActivitatFisica;
                 Database.Instance.InsertObject(Tables.REGISTERED_USERS, users);
             }
         }*/
        public void Save()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/hactivitatfisica", string.Empty));
            HttpResponseMessage response = null;

            foreach (KeyValuePair<string, string> entry in HActivitatFisica)
            {
                var haf = new HActivitatFisicaObject() { pregunta = entry.Key, resposta = entry.Value, user_id = new UserAccount() { id = GetUserId() } };
                var json = JsonConvert.SerializeObject(haf);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                response = Task.Run(() => client.PostAsync(uri, content)).Result;
            }

        }

        long GetUserId()
        {
            //UserAccount currentUser;
            //currentUser = Task.Run(() => Database.Instance.GetObject<UserAccount>(Tables.CURRENT_USER)).Result;

            return CurrentUser.currentUser.id;
        }
    }

    public static class actFisica_survey
    {
        public static List<string> preguntes = new List<string>();
        public static List<string> exemples = new List<string>();
        public static List<string> labels = new List<string>();

        public static List<List<string>> sources = new List<List<string>>();

        public static int index;
        public static double progress;
    }
}
