﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AwesomeApp.Common;
using AwesomeApp.Models;
using AwesomeApp.ViewModels;
using ModernHttpClient;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace AwesomeApp.ViewModels
{
    public class GoldbergViewModel : ObservableBase
    {
        public GoldbergViewModel()
        {
            OptionsPack1 = new List<Options>() { 
                new Options(AppStrings.GoldbergPack1Answer1, "Next.png"),
                new Options(AppStrings.GoldbergPack1Answer2, "Next.png"),
                new Options(AppStrings.GoldbergPack1Answer3, "Next.png"),
                new Options(AppStrings.GoldbergPack1Answer4, "Next.png"),
            };
            OptionsPack2 = new List<Options>() {
                new Options(AppStrings.GoldbergPack2Answer1, "Next.png"),
                new Options(AppStrings.GoldbergPack2Answer2, "Next.png"),
                new Options(AppStrings.GoldbergPack2Answer3, "Next.png"),
                new Options(AppStrings.GoldbergPack2Answer4, "Next.png"),
            };
            OptionsPack3 = new List<Options>() {
                new Options(AppStrings.GoldbergPack3Answer1, "Next.png"),
                new Options(AppStrings.GoldbergPack3Answer2, "Next.png"),
                new Options(AppStrings.GoldbergPack3Answer3, "Next.png"),
                new Options(AppStrings.GoldbergPack3Answer4, "Next.png"),
            };
            OptionsPack4 = new List<Options>() {
                new Options(AppStrings.GoldbergPack4Answer1, "Next.png"),
                new Options(AppStrings.GoldbergPack4Answer2, "Next.png"),
                new Options(AppStrings.GoldbergPack4Answer3, "Next.png"),
                new Options(AppStrings.GoldbergPack4Answer4, "Next.png"),
            };
            OptionsPack5 = new List<Options>() {
                new Options(AppStrings.GoldbergPack5Answer1, "Next.png"),
                new Options(AppStrings.GoldbergPack5Answer2, "Next.png"),
                new Options(AppStrings.GoldbergPack5Answer3, "Next.png"),
                new Options(AppStrings.GoldbergPack5Answer4, "Next.png"),
            };
            OptionsPack6 = new List<Options>() {
                new Options(AppStrings.GoldbergPack6Answer1, "Next.png"),
                new Options(AppStrings.GoldbergPack6Answer2, "Next.png"),
                new Options(AppStrings.GoldbergPack6Answer3, "Next.png"),
                new Options(AppStrings.GoldbergPack6Answer4, "Next.png"),
            };

            goldberg_survey.preguntes = new List<string>()
            {
                AppStrings.GoldbergQuestion1,
                AppStrings.GoldbergQuestion2,
                AppStrings.GoldbergQuestion3,
                AppStrings.GoldbergQuestion4,
                AppStrings.GoldbergQuestion5,
                AppStrings.GoldbergQuestion6,
                AppStrings.GoldbergQuestion7,
                AppStrings.GoldbergQuestion8,
                AppStrings.GoldbergQuestion9,
                AppStrings.GoldbergQuestion10,
                AppStrings.GoldbergQuestion11,
                AppStrings.GoldbergQuestion12,
   

            };

            goldberg_survey.options = new List<List<Options>>() { 
                OptionsPack1,
                OptionsPack2,
                OptionsPack3,
                OptionsPack4,
                OptionsPack2,
                OptionsPack2,
                OptionsPack4,
                OptionsPack5,
                OptionsPack2,
                OptionsPack2,
                OptionsPack2,
                OptionsPack6

            };

            goldberg_survey.index = 0;
            Pregunta = goldberg_survey.preguntes.ElementAt(goldberg_survey.index);
            GoldbergOptions = goldberg_survey.options.ElementAt(goldberg_survey.index);
            goldberg_survey.progress = 0.083;
            Progress = goldberg_survey.progress;

            MalestarPsicologic = 0;

        }
        GoldbergSurvey _gs = new GoldbergSurvey();
        public GoldbergSurvey GS
        {
            get { return _gs; }
            set { SetProperty(ref _gs, value); }
        }

        static Dictionary<string, string> _goldberg;
        public  Dictionary<string, string> Goldberg
        {
            get { return _goldberg; }
            set { SetProperty(ref _goldberg, value); }
        }
        public double _progress;
        public double Progress
        {
            get { return _progress; }
            set { SetProperty(ref _progress, value); }
        }
        public string _pregunta;
        public string Pregunta
        {
            get { return _pregunta; }
            set { SetProperty(ref _pregunta, value); }
        }

        public List<Options> _goldbergOptions;
        public List<Options> GoldbergOptions
        {
            get { return _goldbergOptions; }
            set { SetProperty(ref _goldbergOptions, value); }
        }

        public List<Options> _optionsPack1;
        public List<Options> OptionsPack1
        {
            get { return _optionsPack1; }
            set { SetProperty(ref _optionsPack1, value); }
        }
        public List<Options> _optionsPack2;
        public List<Options> OptionsPack2
        {
            get { return _optionsPack2; }
            set { SetProperty(ref _optionsPack2, value); }
        }
        public List<Options> _optionsPack3;
        public List<Options> OptionsPack3
        {
            get { return _optionsPack3; }
            set { SetProperty(ref _optionsPack3, value); }
        }
        public List<Options> _optionsPack4;
        public List<Options> OptionsPack4
        {
            get { return _optionsPack4; }
            set { SetProperty(ref _optionsPack4, value); }
        }
        public List<Options> _optionsPack5;
        public List<Options> OptionsPack5
        {
            get { return _optionsPack5; }
            set { SetProperty(ref _optionsPack5, value); }
        }
        public List<Options> _optionsPack6;
        public List<Options> OptionsPack6
        {
            get { return _optionsPack6; }
            set { SetProperty(ref _optionsPack6, value); }
        }
        public int _malestarPsicologic;
        public int MalestarPsicologic
        {
            get { return _malestarPsicologic; }
            set { SetProperty(ref _malestarPsicologic, value); }
        }
        public void Handle_Questions_Answers(string answer)
        {
            Goldberg[Pregunta] = answer;

            #region Count critical answers: when are 3d or 4th

            if (answer.Equals(AppStrings.GoldbergPack1Answer3))
            {
                MalestarPsicologic++;
            }
            else if (answer.Equals(AppStrings.GoldbergPack1Answer4))
            {
                MalestarPsicologic++;
            }
            else if (answer.Equals(AppStrings.GoldbergPack2Answer3))
            {
                MalestarPsicologic++;
            }
            else if (answer.Equals(AppStrings.GoldbergPack2Answer4))
            {
                MalestarPsicologic++;
            }
            else if (answer.Equals(AppStrings.GoldbergPack3Answer3))
            {
                MalestarPsicologic++;
            }
            else if (answer.Equals(AppStrings.GoldbergPack3Answer4))
            {
                MalestarPsicologic++;
            }
            else if (answer.Equals(AppStrings.GoldbergPack4Answer3))
            {
                MalestarPsicologic++;
            }
            else if (answer.Equals(AppStrings.GoldbergPack4Answer4))
            {
                MalestarPsicologic++;
            }
            else if (answer.Equals(AppStrings.GoldbergPack5Answer3))
            {
                MalestarPsicologic++;
            }
            else if (answer.Equals(AppStrings.GoldbergPack5Answer4))
            {
                MalestarPsicologic++;
            }
            else if (answer.Equals(AppStrings.GoldbergPack6Answer3))
            {
                MalestarPsicologic++;
            }
            else if (answer.Equals(AppStrings.GoldbergPack6Answer4))
            {
                MalestarPsicologic++;
            }
            #endregion

            if (goldberg_survey.index < 11)
                {
                    goldberg_survey.index++;

                Pregunta = goldberg_survey.preguntes.ElementAt(goldberg_survey.index);
                GoldbergOptions = goldberg_survey.options.ElementAt(goldberg_survey.index);
			}
                
                goldberg_survey.progress += 0.083;
                Progress = goldberg_survey.progress;
           }


        /* public async Task SaveGoldbergAsync()
         {
             String currentUser;
             currentUser = await Database.Instance.GetObject<String>(Tables.CURRENT_USER);

             List<UserAccount> users;
             users = await Database.Instance.GetObject<List<UserAccount>>(Tables.REGISTERED_USERS);

             if (users != null)
             {
                 UserAccount result = users.Find(x => x.mUsername == currentUser);

                 GS.Date = DateTime.Now;

                 GS.GoldbergQA = Goldberg;

                 if (MalestarPsicologic >= 3)
                 {                   
                     GS.MalestarPsicologic = true;
                 }
                 else
                 {
                     GS.MalestarPsicologic = false;
                 }

                 users.ElementAt(users.IndexOf(users.Find(x => x.mUsername == currentUser))).GoldbergStats.Add(GS);
                 Database.Instance.InsertObject(Tables.REGISTERED_USERS, users);
             }
         }*/
        public void Save()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/registerGoldbergRes", string.Empty));
            HttpResponseMessage response = null;

            GS.date = DateTime.Now;

            if (MalestarPsicologic >= 3)
            {
                GS.malestarPsicologic = true;
            }
            else
            {
                GS.malestarPsicologic = false;
            }
            GS.user_id = new UserAccount() { id = GetUserId() } ;

            var json = JsonConvert.SerializeObject(GS);

            var content = new StringContent(json, Encoding.UTF8, "application/json");

            response = Task.Run(() => client.PostAsync(uri, content)).Result;
            
        }

        long GetUserId()
        {
            //UserAccount currentUser;
            //currentUser = Task.Run(() => Database.Instance.GetObject<UserAccount>(Tables.CURRENT_USER)).Result;

            return CurrentUser.currentUser.id;
        }
    }


public static class goldberg_survey
{
    public static List<string> preguntes = new List<string>();
    public static int index;
    public static double progress;
    
    public static List<List<Options>> options = new List<List<Options>>();
}

}


