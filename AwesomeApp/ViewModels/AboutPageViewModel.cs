﻿using AwesomeApp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwesomeApp.ViewModels
{
    class AboutPageViewModel: ObservableBase
    {
        public AboutPageViewModel()
        {
           
            ImagesAttribution = new List<Images>();

            ImagesAttribution.Add(new Images() { Author = "Gregor Cresnar", Source = "Noun Project", Image="clickForInfo.png"});
            ImagesAttribution.Add(new Images() { Author = "Waleed Elagamy", Source = "Noun Project", Image = "BurnOut.png" });
            ImagesAttribution.Add(new Images() { Author = "Orun Bhuiyan", Source = "Noun Project", Image = "HabitsOfLife.png" });
            ImagesAttribution.Add(new Images() { Author = "Eagle Eye", Source = "Noun Project", Image = "LogOut.png" });
            ImagesAttribution.Add(new Images() { Author = "Viktor Vorobyev", Source = "Noun Project", Image = "home.png" });
            ImagesAttribution.Add(new Images() { Author = "Creative Stall", Source = "Noun Project", Image = "noun_Email_111026.png" });
            ImagesAttribution.Add(new Images() { Author = "Aneeque Ahmed", Source = "Noun Project", Image = "noun_call_1482291.png" });
        }

        public List<Images> _imagesAttributtion;
        public List<Images> ImagesAttribution
        {
            get { return _imagesAttributtion; }
            set
            {
                SetProperty(ref _imagesAttributtion, value);
            }
        }
        public Boolean _objectivesvisible = false;
        public Boolean ObjectivesVisible
        {
            get { return _objectivesvisible; }
            set { SetProperty(ref _objectivesvisible, value); }
        }

        public Boolean _veracityvisible = false;
        public Boolean VeracityVisible
        {
            get { return _veracityvisible; }
            set { SetProperty(ref _veracityvisible, value); }
        }
        public Boolean _imagesvisible = false;
        public Boolean ImagesVisible
        {
            get { return _imagesvisible; }
            set { SetProperty(ref _imagesvisible, value); }
        }

        public String _objectivesicon = "mostarmas.png";
        public String ObjectivesIcon
        {
            get { return _objectivesicon; }
            set { SetProperty(ref _objectivesicon, value); }
        }
        public String _veracityicon = "mostarmas.png";
        public String VeracityIcon
        {
            get { return _veracityicon; }
            set { SetProperty(ref _veracityicon, value); }
        }
        public String _imagesicon = "mostarmas.png";
        public String ImagesIcon 
        {
            get { return _imagesicon; }
            set { SetProperty(ref _imagesicon, value); }
        }
        public void ShowObjectives()
        {
            if (!ObjectivesVisible)
            {
                ObjectivesVisible = true;
                ObjectivesIcon = "esconder.png";
            }
            else
            {
                ObjectivesVisible = false;
                ObjectivesIcon = "mostarmas.png";
            }
        }

        public void ShowVeracity()
        {
            if (!VeracityVisible)
            {
                VeracityVisible = true;
                VeracityIcon = "esconder.png";
            }
            else
            {
                VeracityVisible = false;
                VeracityIcon = "mostarmas.png";
            }
        }

        public void ShowImages()
        {
            if (!ImagesVisible)
            {
                ImagesVisible = true;
                ImagesIcon = "esconder.png";
            }
            else
            {
                ImagesVisible = false;
                ImagesIcon = "mostarmas.png";
            }
        }
    }
    public class Images
    {
        public string Author
        {
            get;
            set;
        }
        public string Source
        {
            get;
            set;
        }
        public string Image
        {
            get;
            set;
        }
    }
}
