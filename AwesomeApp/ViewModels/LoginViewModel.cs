﻿using System;
using System.Collections.Generic;
using AwesomeApp.Models;
using AwesomeApp.ViewModels;
using System.Linq;

using AwesomeApp.Common;
using Xamarin.Forms;
using Akavache;
using System.Threading.Tasks;
using System.Net.Http;
using ModernHttpClient;
using Newtonsoft.Json;
using System.Text;

namespace AwesomeApp.ViewModels
{
    public class LoginViewModel : ObservableBase
    {
        
        public LoginViewModel(){
        }

        public string Username { get; set; }
        public string Password { get; set; }

        public bool DataIsNotEmpty => !string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password);


       /* public async Task<bool> CheckLoginAsync()
        {
            List<UserAccount> users;
            users = await Database.Instance.GetObject<List<UserAccount>>(Tables.REGISTERED_USERS);

            if (users != null)
            {
                foreach (var u in users)
                {
                    if (u.password.Equals(Password) && u.mUsername.Equals(Username))
                    {
                        return true;
                    }
                }

			}
                return false;
        }*/
        public Boolean Login()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/login", string.Empty));
            HttpResponseMessage response = null;

            UserAccount user = new UserAccount { mUsername=Username, password=Password};
            var json = JsonConvert.SerializeObject(user);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            response = Task.Run(() => client.PostAsync(uri, content)).Result;

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }
        }


    }
