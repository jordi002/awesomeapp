﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AwesomeApp.Common;
using AwesomeApp.Models;
using ModernHttpClient;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace AwesomeApp.ViewModels
{
	public class HToxicsViewModel : ObservableBase
	{
		public HToxicsViewModel ()
		{

            OptionsPack = new List<Options>() {
                new Options("Si", "Next.png"),
                new Options("No", "Next.png"),
               
            };
           

            toxics_survey.preguntes = new List<string>()
            {
                AppStrings.HToxics_Question1,
                AppStrings.HToxics_Question2,
                AppStrings.HToxics_Question3,
                AppStrings.HToxics_Question4,
                AppStrings.HToxics_Question5,
  
            };



            toxics_survey.index = 0;
            Pregunta = toxics_survey.preguntes.ElementAt(toxics_survey.index);
            Pregunta_freq = AppStrings.HToxics_QuestionFreq;
            toxics_survey.progress = 0.2;
            Progress = toxics_survey.progress;
        }

        public static Dictionary<string, string> _htoxics = new Dictionary<string, string>();
        public Dictionary<string, string> HToxics
        {
            get { return _htoxics; }
            set { SetProperty(ref _htoxics, value); }
        }

        public double _progress;
        public double Progress
        {
            get { return _progress; }
            set { SetProperty(ref _progress, value); }
        }
        public string _pregunta;
        public string Pregunta
        {
            get { return _pregunta; }
            set { SetProperty(ref _pregunta, value); }
        }
        public string _pregunta_freq;
        public string Pregunta_freq
        {
            get { return _pregunta_freq; }
            set { SetProperty(ref _pregunta_freq, value); }
        }
         public List<Options> _optionsPack;
         public List<Options> OptionsPack
        {
            get { return _optionsPack; }
            set { SetProperty(ref _optionsPack, value); }
        }
        
        public void Handle_Questions_Answers(string answer)
        {

            HToxics[Pregunta] = answer;

            if (toxics_survey.index < 4)
            {
                toxics_survey.index++;

                Pregunta = toxics_survey.preguntes.ElementAt(toxics_survey.index);
            }

            toxics_survey.progress += 0.2;
            Progress = toxics_survey.progress;
        }
        public void Handle_Answers_Yes(string answer)
        {
            HToxics[Pregunta + Pregunta_freq] = answer;

            if (toxics_survey.index < 4)
            {
                toxics_survey.index++;

                Pregunta = toxics_survey.preguntes.ElementAt(toxics_survey.index);
            }

            toxics_survey.progress += 0.2;
            Progress = toxics_survey.progress;
        }

        /* public async Task SaveHToxicsAsync()
         {
             String currentUser;
             currentUser = await Database.Instance.GetObject<String>(Tables.CURRENT_USER);

             List<UserAccount> users;
             users = await Database.Instance.GetObject<List<UserAccount>>(Tables.REGISTERED_USERS);

             if (users != null)
             {
                 UserAccount result = users.Find(x => x.Username == currentUser);
                 users.ElementAt(users.IndexOf(users.Find(x => x.Username == currentUser))).HToxics = HToxics;
                 Database.Instance.InsertObject(Tables.REGISTERED_USERS, users);
             }
         }*/

        public void Save()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/htoxics", string.Empty));
            HttpResponseMessage response = null;

            foreach (KeyValuePair<string, string> entry in HToxics)
            {
                var ht = new HActivitatFisicaObject() { pregunta = entry.Key, resposta = entry.Value, user_id = new UserAccount() { id = GetUserId() } };
                var json = JsonConvert.SerializeObject(ht);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                response = Task.Run(() => client.PostAsync(uri, content)).Result;
            }

        }

        long GetUserId()
        {
            //UserAccount currentUser;
            //currentUser = Task.Run(() => Database.Instance.GetObject<UserAccount>(Tables.CURRENT_USER)).Result;

            return CurrentUser.currentUser.id;
        }
    }


    public static class toxics_survey
    {
        public static List<string> preguntes = new List<string>();
        public static int index;
        public static double progress;

    }
}
