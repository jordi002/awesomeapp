﻿using AwesomeApp.Common;
using AwesomeApp.Models;
using ModernHttpClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AwesomeApp.ViewModels
{
    class HomePageViewModel : ObservableBase
    {
        public HomePageViewModel()
        {
        }

        public List<BurnOutSurvey> _burnoutstats = new List<BurnOutSurvey>();
        public List<BurnOutSurvey> BurnOutStats
        {
            get { return _burnoutstats; }
            set { SetProperty(ref _burnoutstats, value); }
        }
        public List<GoldbergSurvey> _goldbergstats = new List<GoldbergSurvey>();
        public List<GoldbergSurvey> GoldbergStats
        {
            get { return _goldbergstats; }
            set { SetProperty(ref _goldbergstats, value); }
        }
        public UserAccount _user = new UserAccount();
        public UserAccount User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        public List<BurnOutSurvey> _dataemoex = new List<BurnOutSurvey>();
        public List<BurnOutSurvey> DataEmoEx
        {
            get { return _dataemoex; }
            set { SetProperty(ref _dataemoex, value); }
        }

        public List<BurnOutSurvey> _datadesp = new List<BurnOutSurvey>();
        public List<BurnOutSurvey> DataDesp
        {
            get { return _datadesp; }
            set { SetProperty(ref _datadesp, value); }
        }
        public List<BurnOutSurvey> _datapf = new List<BurnOutSurvey>();
        public List<BurnOutSurvey> DataPF
        {
            get { return _datapf; }
            set { SetProperty(ref _datapf, value); }
        }

        public List<BurnOutSurvey> _lastdataemoex = new List<BurnOutSurvey>();
        public List<BurnOutSurvey> LastDataEmoEx
        {
            get { return _lastdataemoex; }
            set { SetProperty(ref _lastdataemoex, value); }
        }
        public List<BurnOutSurvey> _lastdatadesp = new List<BurnOutSurvey>();
        public List<BurnOutSurvey> LastDataDesp
        {
            get { return _lastdatadesp; }
            set { SetProperty(ref _lastdatadesp, value); }
        }
        public List<BurnOutSurvey> _lastdatapf = new List<BurnOutSurvey>();
        public List<BurnOutSurvey> LastDataPF
        {
            get { return _lastdatapf; }
            set { SetProperty(ref _lastdatapf, value); }
        }
        public String _name;
        public String Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        public String _imagelastemoex="";
        public String ImageLastEmoEx
        {
            get { return _imagelastemoex; }
            set { SetProperty(ref _imagelastemoex, value); }
        }
        public String _imagelastdesp="";
        public String ImageLastDesp
        {
            get { return _imagelastdesp; }
            set { SetProperty(ref _imagelastdesp, value); }
        }
        public String _imagelastpf="";
        public String ImageLastPF
        {
            get { return _imagelastpf; }
            set { SetProperty(ref _imagelastpf, value); }
        }
        public String _malestar;
        public String Malestar
        {
            get { return _malestar; }
            set { SetProperty(ref _malestar, value); }
        }
        public Boolean _showlastburnout = true;
        public Boolean ShowLastBurnOut
        {
            get { return _showlastburnout; }
            set { SetProperty(ref _showlastburnout, value); }
        }
        public Boolean _showburnoutabsence = false;
        public Boolean ShowBurnOutAbsence
        {
            get { return _showburnoutabsence; }
            set { SetProperty(ref _showburnoutabsence, value); }
        }
        public String _frame2image;
        public String Frame2Image
        {
            get { return _frame2image; }
            set { SetProperty(ref _frame2image, value); }
        }
        public String _frame2color;
        public String Frame2Color
        {
            get { return _frame2color; }
            set { SetProperty(ref _frame2color, value); }
        }

        public Boolean _frame2enable = false;
        public Boolean Frame2Enable
        {
            get { return _frame2enable; }
            set { SetProperty(ref _frame2enable, value); }
        }

        public void GetUserData()
        {
            //UserAccount currentUser;
            //currentUser = Task.Run(() => Database.Instance.GetObject<UserAccount>(Tables.CURRENT_USER)).Result;

            User.id = CurrentUser.currentUser.id;
            User.nom = CurrentUser.currentUser.nom;
            GetGoldbergStats();
            GetBurnoutStats();

            SetData();
        }
        void GetGoldbergStats()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/user/" + User.id + "/goldbergresults", string.Empty));

            var response = Task.Run(() => client.GetAsync(uri)).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                User.GoldbergStats = JsonConvert.DeserializeObject<List<GoldbergSurvey>>(content);

            }
        }
        void GetBurnoutStats()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/user/" + User.id + "/maslachresults", string.Empty));

            var response = Task.Run(() => client.GetAsync(uri)).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                User.BurnOutStats = JsonConvert.DeserializeObject<List<BurnOutSurvey>>(content);

            }
        }
        public void SetData ()
        {

            GoldbergStats = User.GoldbergStats;
            BurnOutStats = User.BurnOutStats;
            Name = AppStrings.HomePageTitle + User.nom;

         
        }
        public void BuildBOCharts()
        {
            foreach (BurnOutSurvey b in BurnOutStats)
            {
                DataEmoEx.Add(new BurnOutSurvey { date = b.date, emotionalExhaust = b.emotionalExhaust, emotionalExhaustValoration = b.emotionalExhaustValoration });
                DataDesp.Add(new BurnOutSurvey { date = b.date, despersonalization = b.despersonalization, despersonalizationValoration = b.despersonalizationValoration });
                DataPF.Add(new BurnOutSurvey { date = b.date, personalFullfillment = b.personalFullfillment, personalFullfillmentValoration = b.personalFullfillmentValoration });
            }

            BurnOutSurvey ultim = new BurnOutSurvey();
            BurnOutSurvey penultim = new BurnOutSurvey();

            ultim = BurnOutStats.ElementAt(BurnOutStats.Count - 1);

            if (ultim.despersonalization == 0 && ultim.emotionalExhaust == 0 && ultim.personalFullfillment == 0)
            {
                ShowLastBurnOut = false;
                ShowBurnOutAbsence = true;
            }
            else
            {

                if (BurnOutStats.Count > 1)
                {
                    penultim = BurnOutStats.ElementAt(BurnOutStats.Count - 2);
                }
                else
                {
                    penultim = null;
                }

                if (penultim != null)
                {
                    if (ultim.emotionalExhaust > penultim.emotionalExhaust)
                    {
                        ImageLastEmoEx = "up.png";
                    }
                    else if (ultim.emotionalExhaust < penultim.emotionalExhaust)
                    {
                        ImageLastEmoEx = "down.png";
                    }

                    if (ultim.despersonalization > penultim.despersonalization)
                    {
                        ImageLastDesp = "up.png";
                    }
                    else if (ultim.despersonalization < penultim.despersonalization)
                    {
                        ImageLastDesp = "down.png";
                    }

                    if (ultim.personalFullfillment > penultim.personalFullfillment)
                    {
                        ImageLastPF = "up.png";
                    }
                    else if (ultim.personalFullfillment < penultim.personalFullfillment)
                    {
                        ImageLastPF = "down.png";
                    }
                }


                LastDataEmoEx.Add(new BurnOutSurvey { date = ultim.date, emotionalExhaust = ultim.emotionalExhaust, Icon = ImageLastEmoEx });
                LastDataDesp.Add(new BurnOutSurvey { date = ultim.date, despersonalization = ultim.despersonalization, Icon = ImageLastDesp });
                LastDataPF.Add(new BurnOutSurvey { date = ultim.date, personalFullfillment = ultim.personalFullfillment, Icon = ImageLastPF });
            }
            if (GoldbergStats.ElementAt(GoldbergStats.Count - 1).malestarPsicologic)
            {
                Malestar = AppStrings.HomePageFram2TitleBad;
                Frame2Image = "care.png";
                Frame2Color = "Black";
                Frame2Enable = true;
            }
            else
            {
                Malestar = AppStrings.HomePageFram2TitleGood;
                Frame2Image = "correct.png";
                Frame2Color = "#002677";
            }
        }


    }
}
