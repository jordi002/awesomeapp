﻿using System;
using Xamarin.Forms;

using AwesomeApp.Views;

namespace AwesomeApp
{
    public partial class MainPage : MasterDetailPage
    {


        public MainPage()
        {

            InitializeComponent();

            Master = masterPage;
            Detail = new NavigationPage(new HomePage());
            NavigationPage.SetHasNavigationBar(this, false);
            masterPage.ListView.ItemSelected += OnItemSelected;


        }
        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterPageItem;
            if (item != null)
            {
                
				Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
               
                masterPage.ListView.SelectedItem = null;
                IsPresented = false;
            }
        }

    }
}
