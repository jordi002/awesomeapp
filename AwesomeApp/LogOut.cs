﻿using System;

using Xamarin.Forms;
using AwesomeApp.Views;
using AwesomeApp.Helpers;

namespace AwesomeApp
{
    public class LogOut : ContentPage
    {
        public LogOut()
        {
            LogOutOfApplication();
        }
        public void LogOutOfApplication()
        {
            //Settings.IsLoggedIn = false;
            Application.Current.MainPage = new NavigationPage(new Login());
        }
       /* protected override void OnAppearing()
        {
            Settings.IsLoggedIn = false;
            Application.Current.MainPage = new NavigationPage(new Login());

        }*/
    }
}

