﻿using Xamarin.Forms;
using AwesomeApp.Views;
using AwesomeApp.Common;
using AwesomeApp.Helpers;
using System.Resources;
using System.Globalization;

namespace AwesomeApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            if (Device.RuntimePlatform == Device.iOS || Device.RuntimePlatform == Device.Android)
            {
         
                var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
                //var ci = new CultureInfo("ca-ES");
                AppStrings.Culture = ci; // set the RESX for resource localization
                DependencyService.Get<ILocalize>().SetLocale(ci); // set the Thread for locale-aware methods
              
            }

           /* if (Settings.IsLoggedIn)
             {
                 MainPage = new NavigationPage(new MainPage());
             }
             else
             {
                 MainPage = new NavigationPage(new Login());
             }*/
           MainPage = new NavigationPage(new Login());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
