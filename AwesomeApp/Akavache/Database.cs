﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Akavache;

namespace AwesomeApp
{
    public class Database
    {
        #region Threadsafe singleton methods
        private static volatile Database instance;
        private static object syncRoot = new Object();
        public static Database Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        instance = new Database();
                    }
                }
                return instance;
            }
        }
        #endregion

        private Database()
        {
            try
            {
                BlobCache.EnsureInitialized();
                BlobCache.ApplicationName = "Dr.Care";
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        public T GetObjectSync<T>(string key)
        {
            try
            {
                return BlobCache.Secure.GetObject<T>(key).Wait();
            }
            catch (KeyNotFoundException)
            {
                return default(T);
            }
        }

        public async Task<T> GetObject<T>(string key)
        {
            try
            {
                return await BlobCache.Secure.GetObject<T>(key);
            }
            catch (KeyNotFoundException)
            {
                return default(T);
            }
        }

        public async Task<Dictionary<string, T>> GetObjects<T>(IEnumerable<string> keys)
        {
            try
            {
                return (Dictionary<string, T>)(await BlobCache.Secure.GetObjects<T>(keys));
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
        }

        public async Task<List<T>> GetAllObjects<T>()
        {
            try
            {
                return (await BlobCache.Secure.GetAllObjects<T>()).ToList();
            }
            catch
            {
                return null;
            }
        }

        public async Task<List<T>> GetAllObjects<T>(Predicate<T> filterPredicate)
        {
            try
            {
                return (await BlobCache.Secure.GetAllObjects<T>()).Where(x => filterPredicate(x)).ToList();
            }
            catch
            {
                return null;
            }
        }

        public bool InsertObject<T>(string key, T value)
        {
            try
            {
                BlobCache.Secure.InsertObject(key, value);
                return true;
            }
            catch (KeyNotFoundException)
            {
                return false;
            }
        }

        public bool RemoveObject<T>(string key)
        {
            try
            {
                BlobCache.Secure.InvalidateObject<T>(key);

                return true;
            }
            catch (KeyNotFoundException)
            {
                return false;
            }
        }

        public bool RemoveAllObjects<T>()
        {
            try
            {
                BlobCache.Secure.InvalidateAllObjects<T>();

                return true;
            }
            catch (KeyNotFoundException)
            {
                return false;
            }
        }

        public bool StoreCredentials(string login, string pwd)
        {
            try
            {
                // This is only local
                BlobCache.Secure.SaveLogin(login, pwd);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public LoginInfo GetStoredCredentials()
        {
            try
            {
                // Block because it is needed in some properties
                return BlobCache.Secure.GetLoginAsync().Wait();
            }
            catch
            {
                return null;
            }
        }

        public bool ResetStoredCredentials()
        {
            try
            {
                // This is only local
                BlobCache.Secure.EraseLogin();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}