﻿using System;
using System.Collections.Generic;
using AwesomeApp.ViewModels;
using AwesomeApp.Models;
using Xamarin.Forms;
using AwesomeApp.Views;
using System.Collections;

namespace AwesomeApp
{
    public partial class BurnOut : ContentPage
    {
        BurnOutViewModel _burnOutViewModel;

        public BurnOut()
        {
            InitializeComponent();

            _burnOutViewModel = new BurnOutViewModel();
            BindingContext = _burnOutViewModel;

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this,false);
            BurnOutAnswers.ItemSelected += OnItemSelected;

        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                Options op = (Options)BurnOutAnswers.SelectedItem;
                BurnOutAnswers.SelectedItem = null;
                _burnOutViewModel.Handle_Questions_Answers(op.Text);
               
                progress.Progress= survey.progress;
                if (survey.index == 18)
				{

                    //_burnOutViewModel.BOS.BurnOutQA = _burnOutViewModel.BurnOut;

                    Application.Current.MainPage = new NavigationPage(new BurnOut_results()
                    {
                        BindingContext = new BurnOutViewModel(_burnOutViewModel.BoPersonal, _burnOutViewModel.BoProfessional, _burnOutViewModel.BoRelational)
                    });

                }
            }
        }
    }
}
