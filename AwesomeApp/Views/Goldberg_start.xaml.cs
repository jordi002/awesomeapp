﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AwesomeApp.Models;
using AwesomeApp.Views;
using ModernHttpClient;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace AwesomeApp
{
    public partial class Goldberg_start : ContentPage
    {
        public Goldberg_start()
        {
           
            InitializeComponent();
        }
        
      
       protected override Boolean OnBackButtonPressed()
        {
            Application.Current.MainPage = new NavigationPage(new MainPage());
            return true;
        }
        void Goldberg_cancel_clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new MainPage());

        }
        
        void Goldberg_start_clicked(object sender, EventArgs e)
        {
            CanDoIt();
            
        }
        UserAccount User = new UserAccount();
        void CanDoIt()
        {
            GetUserData();
            DateTime data1 = User.GoldbergStats[User.GoldbergStats.Count - 1].date;
            DateTime data2 = User.GoldbergStats[User.GoldbergStats.Count - 1].date.AddMinutes(2);
            System.Diagnostics.Debug.WriteLine("********************    Data Ultima Enquesta -> " + data1);
            System.Diagnostics.Debug.WriteLine("********************    Data Ultima Enquesta + 2 Minuts -> " + data2);

            if (data2 > DateTime.Now)
            {
                DisplayAlert("INFO", AppStrings.SurveyException, AppStrings.EmotionalInfoOk);
                Application.Current.MainPage = new NavigationPage(new MainPage());          
            }
            else
            {

            Application.Current.MainPage = new NavigationPage(new Goldberg());
            }

        }
        void GetUserData()
        {
            User.id = GetUserId();
            GetGoldbergStats();
        }
        long GetUserId()
        {
            //UserAccount currentUser;
            //currentUser = Task.Run(() => Database.Instance.GetObject<UserAccount>(Tables.CURRENT_USER)).Result;

            return CurrentUser.currentUser.id;
        }
        void GetGoldbergStats()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/user/" + User.id + "/goldbergresults", string.Empty));

            var response = Task.Run(() => client.GetAsync(uri)).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                User.GoldbergStats = JsonConvert.DeserializeObject<List<GoldbergSurvey>>(content);

            }
        }
    }
}
