﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AwesomeApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BurnOut_start_FirstTime : ContentPage
	{
		public BurnOut_start_FirstTime ()
		{
			InitializeComponent ();
		}
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        void Emotional_Clicked(object sender, EventArgs args)
        {
            DisplayAlert("", AppStrings.EmotionalInfo, AppStrings.EmotionalInfoOk);

        }

        void Despersonalization_Clicked(object sender, EventArgs args)
        {
            DisplayAlert("", AppStrings.DespersonalizationInfo, AppStrings.DespersonalizationInfoOk);

        }

        void Fullfillment_Clicked(object sender, EventArgs args)
        {
            DisplayAlert("", AppStrings.FullfillmentInfo, AppStrings.FullfillmentInfoOk);

        }
        private void BurnOut_start_clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new BurnOut_Diagnostic());
        }
    }
}