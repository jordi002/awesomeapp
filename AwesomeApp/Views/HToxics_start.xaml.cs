﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AwesomeApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HToxics_start : ContentPage
    {
        public HToxics_start()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        private void HToxics_start_clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new HToxics());
        }
    }
}