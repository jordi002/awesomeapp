﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AwesomeApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Goldberg_start_FirstTime : ContentPage
	{
		public Goldberg_start_FirstTime ()
		{
			InitializeComponent ();
		}
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        void Goldberg_start_clicked(object sender, EventArgs e)
        {

            Application.Current.MainPage = new NavigationPage(new Goldberg_FirstTime());
        }
    }
}