﻿using AwesomeApp.ViewModels;
using AwesomeApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AwesomeApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Goldberg_FirstTime : ContentPage
	{
        GoldbergViewModel _goldbergViewModel;
        public Goldberg_FirstTime ()
		{
            InitializeComponent();

            _goldbergViewModel = new GoldbergViewModel();
            BindingContext = _goldbergViewModel;


            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            _goldbergViewModel.Goldberg = new Dictionary<string, string>();

            GoldbergAnswers.ItemSelected += OnItemSelected;

        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                Options op = (Options)GoldbergAnswers.SelectedItem;
                GoldbergAnswers.SelectedItem = null;
                _goldbergViewModel.Handle_Questions_Answers(op.Text);

                progress.Progress = goldberg_survey.progress;

                if (_goldbergViewModel.Goldberg.Count == 12)
                {

                    /*if (_goldbergViewModel.MalestarPsicologic >= 3)
                    {
                        DisplayAlert("INFO", AppStrings.GoldbergPopUpContent, AppStrings.EmotionalInfoOk);
                    }*/
                    //SaveAsync();
                    _goldbergViewModel.Save();
                    Application.Current.MainPage = new NavigationPage(new BurnOut_start_FirstTime());
                }
            }
        }
        /*async void SaveAsync()
        {
            await _goldbergViewModel.SaveGoldbergAsync();
            Application.Current.MainPage = new NavigationPage(new BurnOut_start_FirstTime());
        }*/
    }
}