﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AwesomeApp.ViewModels;
using AwesomeApp.Views;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AwesomeApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HActivitatFisica : ContentPage
    {
        HActFisicaViewModel _hafViewModel;
        public HActivitatFisica()
        {
            _hafViewModel = new HActFisicaViewModel();
            BindingContext = _hafViewModel;

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            InitializeComponent();
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
         void SaveAndContinueAsync(object sender, EventArgs args)
        {
            
            if (HAFpicker.SelectedItem != null)
            {

                if (_hafViewModel.Pregunta.Equals(actFisica_survey.preguntes[5]) ||
                    _hafViewModel.Pregunta.Equals(actFisica_survey.preguntes[4]) && HAFpicker.SelectedItem.Equals("0"))
                {
                    _hafViewModel.Handle_Questions_Answers(HAFpicker.SelectedItem.ToString());

                    progress.Progress = actFisica_survey.progress;
                    //SaveAsync();
                    _hafViewModel.Save();
                    Application.Current.MainPage = new NavigationPage(new HToxics_start());
                }
                else
                {
                    _hafViewModel.Handle_Questions_Answers(HAFpicker.SelectedItem.ToString());

                    progress.Progress = actFisica_survey.progress;
                }
            }

        }
        /*async void SaveAsync()
        {
            await _hafViewModel.SaveHActivitatFisicaAsync();
        }*/
    }
}