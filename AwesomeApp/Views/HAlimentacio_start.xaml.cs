﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using AwesomeApp.Views;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AwesomeApp
{
        
   
    public partial class HAlimentacio_start : ContentPage
    {
        public HAlimentacio_start()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        async void MoreInfo_Clicked(object sender, EventArgs args)
        {
            await DisplayAlert("", AppStrings.HAlimentacioInfo, AppStrings.HAlimentacioInfoOk);

        }

        private async void HAlimentacio_start_clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new HAlimentacio());
        }
    }
}