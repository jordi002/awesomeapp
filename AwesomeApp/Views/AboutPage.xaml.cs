﻿using AwesomeApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AwesomeApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        AboutPageViewModel _aboutPageViewModel;
        public AboutPage()
        {
            _aboutPageViewModel = new AboutPageViewModel();
            BindingContext = _aboutPageViewModel;
            InitializeComponent();

        }
        protected override Boolean OnBackButtonPressed()
        {
            Application.Current.MainPage = new NavigationPage(new MainPage());
            return true;
        }
        private void Objectives(object sender, EventArgs e)
        {
            _aboutPageViewModel.ShowObjectives();
        }

        private void Veracity(object sender, EventArgs e)
        {
            _aboutPageViewModel.ShowVeracity();
        }

        private void Imatges(object sender, EventArgs e)
        {
            _aboutPageViewModel.ShowImages();
        }

        private void Contact(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("mailto:alcarras.jordi.godia@gmail.com?subject=CONTACT"));
        }
    }
}