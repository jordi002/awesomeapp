﻿using AwesomeApp.ViewModels;
using AwesomeApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AwesomeApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChangePassword : ContentPage
    {
        RecoveryPassViewModel _RecoveryPassViewModel;
        public ChangePassword()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            _RecoveryPassViewModel = new RecoveryPassViewModel();
            BindingContext = _RecoveryPassViewModel;
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        private void createNewPwd(object sender, EventArgs e)
        {
            _RecoveryPassViewModel.Password1 = pwd1.Text;
            _RecoveryPassViewModel.Password2 = pwd2.Text;

            if (_RecoveryPassViewModel.DataIsNotEmpty2)
            {
                if (_RecoveryPassViewModel.Password1.Equals(_RecoveryPassViewModel.Password2))
                {
                    if (_RecoveryPassViewModel.PasswordFormatIsOk())
                    {
                        _RecoveryPassViewModel.SaveNewPassword();
                        Application.Current.MainPage = new NavigationPage(new Login());
                    }
                    else
                    {
                        DisplayAlert("Error", AppStrings.WrongPasswordFormat, "OK");

                    }
                }
                else
                {
                    DisplayAlert("Error", AppStrings.DifferentPasswords, "OK");
                }

            }
            else
            {
                DisplayAlert("Error", AppStrings.ErrorEmptyFields, "OK");
            }
        }
        private void CancelRecover(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Login());
        }
    }
}