﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AwesomeApp.Models;
using AwesomeApp.ViewModels;
using AwesomeApp.Views;
using Xamarin.Forms;

namespace AwesomeApp
{
    public partial class Goldberg : ContentPage
    {
        GoldbergViewModel _goldbergViewModel;
        public Goldberg()
        {
			InitializeComponent();

            _goldbergViewModel = new GoldbergViewModel();
            BindingContext = _goldbergViewModel;

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            _goldbergViewModel.Goldberg = new Dictionary<string, string>();
            
            GoldbergAnswers.ItemSelected += OnItemSelected;
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                Options op = (Options)GoldbergAnswers.SelectedItem;
                GoldbergAnswers.SelectedItem = null;
                _goldbergViewModel.Handle_Questions_Answers(op.Text);

                progress.Progress = goldberg_survey.progress;
              
                if(_goldbergViewModel.Goldberg.Count == 12){

                    if (_goldbergViewModel.MalestarPsicologic >= 3)
                    {
                        DisplayAlert("INFO", AppStrings.GoldbergPopUpContent, AppStrings.EmotionalInfoOk);
                    }
                    //SaveAsync();
                    _goldbergViewModel.Save();
                    Application.Current.MainPage = new NavigationPage(new MainPage());
                }
            }
        }

        /*async void SaveAsync()
        {
            await _goldbergViewModel.SaveGoldbergAsync();
            Application.Current.MainPage = new NavigationPage(new MainPage());
        }*/

    }
}
