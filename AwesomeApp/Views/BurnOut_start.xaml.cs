﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AwesomeApp.Models;
using AwesomeApp.Views;
using ModernHttpClient;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace AwesomeApp
{
    public partial class BurnOut_start : ContentPage
    {
        public BurnOut_start()
        {
            InitializeComponent();

        }

        protected override Boolean OnBackButtonPressed()
        {
            Application.Current.MainPage = new NavigationPage(new MainPage());
            return true;
        }
        void Emotional_Clicked(object sender, EventArgs args)
        {
             DisplayAlert("", AppStrings.BurnOutStartPersonalDescription, AppStrings.EmotionalInfoOk);
          
        }

        void Despersonalization_Clicked(object sender, EventArgs args)
        {
             DisplayAlert("", AppStrings.BurnOutStartProfessionalDescription, AppStrings.DespersonalizationInfoOk);

        }

        void Fullfillment_Clicked(object sender, EventArgs args)
        {
             DisplayAlert("", AppStrings.BurnOutStartRelationalDescription, AppStrings.FullfillmentInfoOk);

        }

  
     
         void BurnOut_cancel_clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new MainPage());

        }

        private  void BurnOut_start_clicked(object sender, EventArgs e)
        {
            CanDoIt();
        }
        UserAccount User = new UserAccount();
        void CanDoIt()
        {
            GetUserData();
            DateTime data1 = User.BurnOutStats[User.BurnOutStats.Count - 1].date;
            DateTime data2 = User.BurnOutStats[User.BurnOutStats.Count - 1].date.AddMinutes(2);
            System.Diagnostics.Debug.WriteLine("********************    Data Ultima Enquesta -> " + data1);
            System.Diagnostics.Debug.WriteLine("********************    Data Ultima Enquesta + 2 Minuts -> " + data2);

            if (data2 > DateTime.Now)
            {
                DisplayAlert("INFO", AppStrings.SurveyException, AppStrings.EmotionalInfoOk);
                Application.Current.MainPage = new NavigationPage(new MainPage());
                
            }
            else
            {

                Application.Current.MainPage = new NavigationPage(new BurnOut());
            }

        }
        void GetUserData()
        {
            User.id = GetUserId();
            GetBurnoutStats();
        }
        long GetUserId()
        {
            //UserAccount currentUser;
            //currentUser = Task.Run(() => Database.Instance.GetObject<UserAccount>(Tables.CURRENT_USER)).Result;

            return CurrentUser.currentUser.id;
        }
        void GetBurnoutStats()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/user/" + User.id + "/maslachresults", string.Empty));

            var response = Task.Run(() => client.GetAsync(uri)).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                User.BurnOutStats = JsonConvert.DeserializeObject<List<BurnOutSurvey>>(content);

            }
        }
    }
}
