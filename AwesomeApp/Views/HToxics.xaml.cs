﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AwesomeApp.Models;
using AwesomeApp.ViewModels;
using AwesomeApp.Views;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AwesomeApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HToxics : ContentPage
	{
        HToxicsViewModel _htViewModel;
		public HToxics ()
		{
            _htViewModel = new HToxicsViewModel();
            BindingContext = _htViewModel;

			InitializeComponent ();


            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);

            ToxicsAnswers.ItemSelected += OnItemSelected;
            AnswersFreq.ItemSelected += OnFreqSelected;
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {           
                    Options op = (Options)ToxicsAnswers.SelectedItem;
                    if (op.Text.Equals("Si"))
                    {
                        _htViewModel.HToxics[_htViewModel.Pregunta] = op.Text;
                        preg1.IsEnabled = false;
                        preg2.IsVisible = true;

                    }
                    else
                    {
                        if (_htViewModel.Pregunta.Equals(toxics_survey.preguntes[4]))
                        {
                            ToxicsAnswers.SelectedItem = null;
                            _htViewModel.Handle_Questions_Answers(op.Text);

                            _htViewModel.Save();
                            //SaveAsync();
                            Application.Current.MainPage = new NavigationPage(new WelcomePage2());
                        }
                        else
                        {
                        ToxicsAnswers.SelectedItem = null;
                        _htViewModel.Handle_Questions_Answers(op.Text);
                        }
                    }

                    progress.Progress = toxics_survey.progress;
                



            }
        }
        void OnFreqSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                if (_htViewModel.Pregunta.Equals(toxics_survey.preguntes[4]))
                {
                    Options op = (Options)AnswersFreq.SelectedItem;
                    ToxicsAnswers.SelectedItem = null;
                    AnswersFreq.SelectedItem = null;
                    _htViewModel.Handle_Answers_Yes(op.Text);
                    preg2.IsVisible = false;
                    preg1.IsEnabled = true;

                    _htViewModel.Save();
                    //SaveAsync();
                    Application.Current.MainPage = new NavigationPage(new WelcomePage2());
                }
                else
                {
                    Options op = (Options)AnswersFreq.SelectedItem;
                    ToxicsAnswers.SelectedItem = null;
                    AnswersFreq.SelectedItem = null;
                    _htViewModel.Handle_Answers_Yes(op.Text);
                    preg2.IsVisible = false;
                    preg1.IsEnabled = true;
                }


            }
        }

        /*async void SaveAsync()
        {
            await _htViewModel.SaveHToxicsAsync();
        }*/
    }
}