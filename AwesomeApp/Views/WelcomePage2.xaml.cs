﻿using AwesomeApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AwesomeApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WelcomePage2 : ContentPage
    {
        WelcomeViewModel _welcomeViewModel;

        public WelcomePage2()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            _welcomeViewModel = new WelcomeViewModel();
            BindingContext = _welcomeViewModel;
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        private void Continue(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Goldberg_start_FirstTime());
        }

        async void MoreInfo_Clicked(object sender, EventArgs args)
        {
            await DisplayAlert("",AppStrings.WelcomePage2PopUpDescription, AppStrings.HAlimentacioInfoOk);
        }
        void OpenUri(object sender, EventArgs args)
        {
            Device.OpenUri(new Uri("https://es.wikipedia.org/wiki/S%C3%ADndrome_de_desgaste_profesional"));


        }
    }
    }