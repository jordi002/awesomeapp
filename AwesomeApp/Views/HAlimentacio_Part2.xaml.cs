﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AwesomeApp.Views;
using AwesomeApp.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AwesomeApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HAlimentacio_Part2 : ContentPage
	{
        HAlimentacioViewModel _haViewModel;

		public HAlimentacio_Part2 ()
		{
            _haViewModel = new HAlimentacioViewModel();
            BindingContext = _haViewModel;
            _haViewModel.Progress = 0.375;



            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);

            InitializeComponent ();

           
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
       async void SaveAndContinueAsync(object sender, EventArgs args)
        {
 
            if (!string.IsNullOrEmpty(racions.SelectedItem.ToString()))
            {
               
                _haViewModel.SavePickerAnswer(racions.SelectedItem.ToString());
                await Navigation.PushAsync(new HAlimentacio_Part3());
            }
            else
            {
                await DisplayAlert("Error", AppStrings.ErrorEmptyFields, "OK");
            }
            
        }
 
    }
}