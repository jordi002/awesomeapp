﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using AwesomeApp.Models;
using AwesomeApp.ViewModels;
using AwesomeApp.Views;
using Org.W3c.Dom;
using Syncfusion.SfChart.XForms;
using Xamarin.Forms;

namespace AwesomeApp
{
    public partial class HomePage : ContentPage
    {
        Boolean isPremium = false;
        HomePageViewModel _homePageViewModel;

        public HomePage()
        {
           _homePageViewModel = new HomePageViewModel();
            BindingContext = _homePageViewModel;
   
            _homePageViewModel.GetUserData();

            _homePageViewModel.BuildBOCharts();
            InitializeComponent();
  
        }

        //public Boolean CanContinue = false;
        //public UserAccount User;

        /*void GetUserData()
        {
            String currentUser;
            currentUser = Task.Run(() => Database.Instance.GetObject<String>(Tables.CURRENT_USER)).Result;

            List<UserAccount> users = new List<UserAccount>();
            users = Task.Run(() => Database.Instance.GetObject<List<UserAccount>>(Tables.REGISTERED_USERS)).Result;
            if (users != null)
            {
                User = users.Find(x => x.mUsername == currentUser);
                _homePageViewModel.SetData(User);
                CanContinue = true;
            }
        }*/

        private void Contact(object sender, EventArgs e)
        {
            if (isPremium)
            {
                //TODO: Update when have official support
                Device.OpenUri(new Uri("mailto:placeholder@mail.com?subject=CONTACT"));
            }
        }
    }
}