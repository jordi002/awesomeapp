﻿using AwesomeApp.Models;
using AwesomeApp.ViewModels;
using ModernHttpClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AwesomeApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Diagnostic : ContentPage
    {
        Boolean isPremium = false;
        BurnOutViewModel _burnOutViewModel;
        public Diagnostic()
        {
            InitializeComponent();

            _burnOutViewModel = new BurnOutViewModel();
            BindingContext = _burnOutViewModel;
            _burnOutViewModel.IsPremium = isPremium;

            //_burnOutViewModel.CalculateResults();
            GetUserData();

            if (CanContinue)
            {

                InitializeComponent();
                ShowDiagnostic();
            }
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        public Boolean CanContinue = false;
        public UserAccount User = new UserAccount();

        public Boolean malestarPsicologic;
        public String EmotionalExhaust;
        public String Depersonalization;
        public String PersonalFulfilment;

        void GetUserData()
        {
            User.id = GetUserId();
            GetGoldbergStats();
            GetBurnoutStats();

            malestarPsicologic = User.GoldbergStats[User.GoldbergStats.Count - 1].malestarPsicologic;
            EmotionalExhaust = User.BurnOutStats[User.BurnOutStats.Count - 1].emotionalExhaustValoration;
            Depersonalization = User.BurnOutStats[User.BurnOutStats.Count - 1].despersonalizationValoration;
            PersonalFulfilment = User.BurnOutStats[User.BurnOutStats.Count - 1].personalFullfillmentValoration;

            CanContinue = true;
            
        }
        long GetUserId()
        {
            //UserAccount currentUser;
            //currentUser = Task.Run(() => Database.Instance.GetObject<UserAccount>(Tables.CURRENT_USER)).Result;

            return CurrentUser.currentUser.id;
        }
        void GetGoldbergStats()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/user/" + User.id + "/goldbergresults", string.Empty));

            var response = Task.Run(() => client.GetAsync(uri)).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                User.GoldbergStats = JsonConvert.DeserializeObject<List<GoldbergSurvey>>(content);

            }
        }
        void GetBurnoutStats()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/user/" + User.id + "/maslachresults", string.Empty));

            var response = Task.Run(() => client.GetAsync(uri)).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                User.BurnOutStats = JsonConvert.DeserializeObject<List<BurnOutSurvey>>(content);

            }
        }
        void ShowDiagnostic()
        {
            getStats();

            if((Bajo == 3 || (Bajo==2 && Moderado == 1)) && !malestarPsicologic)
            {
                HighRisk.IsVisible = false;
                ModeratedRisk.IsVisible = false;
                LowRisk.IsVisible = true;
                Incongruent.IsVisible = false;
            }else if (Moderado == 3 || (Moderado==2 && Bajo==1) ||Elevado==1)
            {
                HighRisk.IsVisible = false;
                ModeratedRisk.IsVisible = true;
                LowRisk.IsVisible = false;
                Incongruent.IsVisible = false;
            }else if (Elevado >= 2 && malestarPsicologic)
            {
                HighRisk.IsVisible = true;
                ModeratedRisk.IsVisible = false;
                LowRisk.IsVisible = false;
                Incongruent.IsVisible = false;
            }
            else
            {
                HighRisk.IsVisible = false;
                ModeratedRisk.IsVisible = false;
                LowRisk.IsVisible = false;
                Incongruent.IsVisible = true;
            }
        }
        public int Elevado = 0;
        public int Moderado = 0;
        public int Bajo = 0;

        void getStats()
        {
            String[] s = new String[]
            {
                EmotionalExhaust,
                Depersonalization,
                PersonalFulfilment,
            };

            foreach(String x in s)
            {
                if(x.Equals(AppStrings.BurnOutResultsValorationHigh) || x.Equals(AppStrings.BurnOutResultsValorationSevere))
                {
                    Elevado++;
                }
                else if(x.Equals(AppStrings.BurnOutResultsValorationMedium))
                {
                    Moderado++;
                }
                else if (x.Equals(AppStrings.BurnOutResultsValorationLow))
                {
                    Bajo++;
                }
            }
        }
        private void GoHome(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new MainPage());

        }

        private void ContactMail(object sender, EventArgs e)
        {
            //TODO: Update when have official support
            if (isPremium) {
                Device.OpenUri(new Uri("mailto:placeholder@mail.com?subject=CONTACT"));
            }
        }

        private void ContactPhone(object sender, EventArgs e)
        {
            //TODO: Update when have official support
            if (isPremium) {
                Device.OpenUri(new Uri("tel:"));
            }
        }

        private void Web(object sender, EventArgs e)
        {
            //TODO: Update when have official support
            if (isPremium) {
                Device.OpenUri(new Uri("http://www.fgalatea.org/"));
            }
        }

        private void WebFormacio(object sender, EventArgs e)
        {
            //TODO: Update when have official support
            if (isPremium) {
                Device.OpenUri(new Uri(" http://www.fgalatea.org/ca/promocio_salut.php"));
            }
           
        }
    }
}