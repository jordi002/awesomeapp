﻿using System;
using System.Collections.Generic;
using System.Linq;
using AwesomeApp.Models;
using AwesomeApp.ViewModels;

using Xamarin.Forms;

namespace AwesomeApp
{
    public partial class BurnOut_results : ContentPage
    {
        BurnOutViewModel _burnOutViewModel;
        public BurnOut_results()
        {
            InitializeComponent();

 //           _burnOutViewModel = new BurnOutViewModel();
 //           BindingContext = _burnOutViewModel;

 //           _burnOutViewModel.CalculateResults();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        protected override  void OnAppearing()
        {
            
            base.OnAppearing();
        }
     
        private  void GoHome(object sender, EventArgs e)
        {
                Application.Current.MainPage = new NavigationPage(new MainPage());
  
        }

    }
}
