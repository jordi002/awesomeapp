﻿using System;
using AwesomeApp.ViewModels;
using AwesomeApp.Models;
using AwesomeApp.Helpers;

using Xamarin.Forms;
using System.Net.Http;
using ModernHttpClient;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;

namespace AwesomeApp.Views
{
    public partial class Login : ContentPage
    {
        LoginViewModel _loginViewModel;

        public Login()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            _loginViewModel = new LoginViewModel();
            BindingContext = _loginViewModel;
        }
        UserAccount User = new UserAccount();
        async void GoHome(object sender, EventArgs e)
        {
            var user = new UserAccount
            {
                mUsername = usernameEntry.Text,
                password = passwordEntry.Text
            };

            _loginViewModel.Username = usernameEntry.Text;
            _loginViewModel.Password = passwordEntry.Text;

            if (_loginViewModel.DataIsNotEmpty)
            {
                //if (await _loginViewModel.CheckLoginAsync())
                if(_loginViewModel.Login())
                {
                    //UserAccount currentUser = new UserAccount { id = GetUserId(), mUsername = User.mUsername, nom = User.nom };
                    //Database.Instance.InsertObject(Tables.CURRENT_USER, currentUser);
                    CurrentUser.currentUser = new UserAccount();
                    CurrentUser.currentUser.id = GetUserId();
                    CurrentUser.currentUser.mUsername = User.mUsername;
                    CurrentUser.currentUser.nom = User.nom;
                    //Settings.IsLoggedIn = true;
                    Application.Current.MainPage = new NavigationPage(new MainPage());                
                }
                else
                {
                    usernameEntry.Text = "";
                    passwordEntry.Text = "";
                       await DisplayAlert("Error", AppStrings.ErrorWrongInput, "OK");
                }

            }
            else
            {
                await DisplayAlert("Error", AppStrings.ErrorEmptyFields, "OK");
            }
        }
        long GetUserId()
        {

            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/user", string.Empty));
            HttpResponseMessage response = null;

            UserAccount u = new UserAccount { mUsername = usernameEntry.Text};
            var json = JsonConvert.SerializeObject(u);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            response = Task.Run(() => client.PostAsync(uri, content)).Result;

            if (response.IsSuccessStatusCode)
            {
                var c = response.Content.ReadAsStringAsync().Result;
                var r = JsonConvert.DeserializeObject<UserAccount>(c);
                User.mUsername = r.mUsername;
                User.nom = r.nom;
                return r.id;
            }
            return 0;
        }
        void OnSignUpClicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new SignUpPage());
        }
         void OnRecoveryClicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new RecoverPage());
        }
    }
}

