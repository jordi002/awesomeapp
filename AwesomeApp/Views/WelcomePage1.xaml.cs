﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AwesomeApp.ViewModels;

namespace AwesomeApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WelcomePage1 : ContentPage
    {
        WelcomeViewModel _welcomeViewModel;

        public WelcomePage1()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            _welcomeViewModel = new WelcomeViewModel();
            BindingContext = _welcomeViewModel;
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        private void Continue(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new HAlimentacio_start());
        }
    }
}