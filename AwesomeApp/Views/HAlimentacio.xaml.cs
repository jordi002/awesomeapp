﻿using AwesomeApp.ViewModels;
using System;


using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AwesomeApp
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HAlimentacio : ContentPage
    {
        HAlimentacioViewModel _haViewModel;
        public HAlimentacio()
        {
            _haViewModel = new HAlimentacioViewModel();
            BindingContext = _haViewModel;
            _haViewModel.Progress = 0.25;

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);

            InitializeComponent();

            BuildSlider();
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        void Slider1_ValueChanged(object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            f1_1.BackgroundColor = Color.Default;
            s1_1.TextColor = Color.Default;
            f1_2.BackgroundColor = Color.Default;
            s1_2.TextColor = Color.Default;
            f1_3.BackgroundColor = Color.Default;
            s1_3.TextColor = Color.Default;
            f1_4.BackgroundColor = Color.Default;
            s1_4.TextColor = Color.Default;
            f1_5.BackgroundColor = Color.Default;
            s1_5.TextColor = Color.Default;

            var newStep = Math.Round(e.NewValue);

            slider.Value = newStep;
            if ((int)slider.Value == 1)
            {
                f1_1.BackgroundColor = Color.FromHex("#7593E9");

            }
            if ((int)slider.Value == 2)
            {
                f1_2.BackgroundColor = Color.FromHex("#7593E9");

            }
            if ((int)slider.Value == 3)
            {
                f1_3.BackgroundColor = Color.FromHex("#7593E9");

            }
            if ((int)slider.Value == 4)
            {
                f1_4.BackgroundColor = Color.FromHex("#7593E9");

            }
            if ((int)slider.Value == 5)
            {
                f1_5.BackgroundColor = Color.FromHex("#7593E9");

            }


        }
        void Slider2_ValueChanged(object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            f2_1.BackgroundColor = Color.Default;
            s2_1.TextColor = Color.Default;
            f2_2.BackgroundColor = Color.Default;
            s2_2.TextColor = Color.Default;
            f2_3.BackgroundColor = Color.Default;
            s2_3.TextColor = Color.Default;
            f2_4.BackgroundColor = Color.Default;
            s2_4.TextColor = Color.Default;
            f2_5.BackgroundColor = Color.Default;
            s2_5.TextColor = Color.Default;

            var newStep = Math.Round(e.NewValue);

            slider2.Value = newStep;
            if ((int)slider2.Value == 1)
            {
                f2_1.BackgroundColor = Color.FromHex("#7593E9");
                
            }
            if ((int)slider2.Value == 2)
            {
                f2_2.BackgroundColor = Color.FromHex("#7593E9");
                
            }
            if ((int)slider2.Value == 3)
            {
                f2_3.BackgroundColor = Color.FromHex("#7593E9");
                
            }
            if ((int)slider2.Value == 4)
            {
                f2_4.BackgroundColor = Color.FromHex("#7593E9");
                
            }
            if ((int)slider2.Value == 5)
            {
                f2_5.BackgroundColor = Color.FromHex("#7593E9");
                
            }
            

        }
        void BuildSlider()
        {
            if (_haViewModel.Values != null)
            {

                slider.Maximum = _haViewModel.Values.Count;
                slider.Minimum = 1;

                slider2.Maximum = _haViewModel.Values.Count;
                slider2.Minimum = 1;

                s1_1.Text = _haViewModel.Values[0];
                s1_2.Text = _haViewModel.Values[1];
                s1_3.Text = _haViewModel.Values[2];
                s1_4.Text = _haViewModel.Values[3];
                s1_5.Text = _haViewModel.Values[4];

                s2_1.Text = _haViewModel.Values[0];
                s2_2.Text = _haViewModel.Values[1];
                s2_3.Text = _haViewModel.Values[2];
                s2_4.Text = _haViewModel.Values[3];
                s2_5.Text = _haViewModel.Values[4];
            }
        }
       void SaveAndContinue(object sender, EventArgs args)
        {
            _haViewModel.SaveSliderAnswers((int)slider.Value, (int)slider2.Value);
            Navigation.PushAsync(new HAlimentacio_Part2());
        }
    }
}