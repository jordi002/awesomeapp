﻿using System;
using System.Collections.Generic;
using AwesomeApp.Models;
using AwesomeApp.ViewModels;
using Xamarin.Forms;
using System.Linq;
using AwesomeApp.Views;

namespace AwesomeApp
{
    public partial class RecoverPage : ContentPage
    {
        RecoveryPassViewModel _RecoveryPassViewModel;

        public RecoverPage()
        {
            InitializeComponent();
          
                NavigationPage.SetHasNavigationBar(this, false);
            
            _RecoveryPassViewModel = new RecoveryPassViewModel();
            BindingContext = _RecoveryPassViewModel;


        }

        protected override Boolean OnBackButtonPressed(){
            Application.Current.MainPage = new NavigationPage(new Login());
            return true;
        }
        async void SendRecover(object sender, EventArgs e)
        {
            RecoveryPassViewModel.Username = usernameEntryRec.Text;
            _RecoveryPassViewModel.Email = emailEntryRec.Text;

            if (_RecoveryPassViewModel.DataIsNotEmpty)
            {


            /*var username = " ";
                var email = " ";

                username = usernameEntryRec.Text;
                email = emailEntryRec.Text.ToLower();

                var user = new UserAccount
                {
                    mUsername = username,
                    mEmail = email
                };*/

                if ( _RecoveryPassViewModel.CheckRecForm())
                {
                    //_RecoveryPassViewModel.SendEmailAsync();
                    //await   DisplayAlert("Info", "Your password is: " + _RecoveryPassViewModel.Password, "OK");
                    //await DisplayAlert("Info", "Recovery mail sent. Please check your mailbox.", "OK");
                    await Navigation.PushModalAsync(new ChangePassword());
                }
                else
                {
                    await DisplayAlert("Error", AppStrings.ErrorWrongInput, "OK");
                }
            }
            else
            {
                await DisplayAlert("Error", AppStrings.ErrorEmptyFields, "OK");
            }
        }
         void CancelRecover(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Login());
        }

    }
}
