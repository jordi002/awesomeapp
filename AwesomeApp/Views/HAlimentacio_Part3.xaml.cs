﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AwesomeApp.Views;
using AwesomeApp.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AwesomeApp.Models;

namespace AwesomeApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HAlimentacio_Part3 : ContentPage
	{
        HAlimentacioViewModel _haViewModel;
		public HAlimentacio_Part3 ()
		{
            _haViewModel = new HAlimentacioViewModel();
            BindingContext = _haViewModel;

            _haViewModel.Progress = 0.5;
            alimentacio_survey.progress = 0.5;
			InitializeComponent ();

            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            HAlimentacioAnswers.ItemSelected += OnItemSelected;
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                Options op = (Options)HAlimentacioAnswers.SelectedItem;
                HAlimentacioAnswers.SelectedItem = null;
                _haViewModel.Handle_Questions_Answers(op.Text);

                progress.Progress = alimentacio_survey.progress;

                if (_haViewModel.HAlimentacio.Count == 8)
                {
                    //SaveAsync();
                    _haViewModel.Save();
                    Application.Current.MainPage = new NavigationPage(new HActivitatFisica_start());
                }
            }
        }

        /*async void SaveAsync()
        {
            await _haViewModel.SaveHAlimentacioAsync();
        }*/
    }
}