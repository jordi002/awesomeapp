﻿using System;
using System.Collections.Generic;
using System.Linq;
using AwesomeApp.Views;
using Xamarin.Forms;
using AwesomeApp.ViewModels;
using AwesomeApp.Models;
using AwesomeApp.Helpers;
using System.Net.Http;
using ModernHttpClient;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;

namespace AwesomeApp
{
    public partial class SignUpPage_Part2 : ContentPage
    {

        SignUpViewModel _SignUpViewModel;

        public SignUpPage_Part2()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            _SignUpViewModel = new SignUpViewModel();
            BindingContext = _SignUpViewModel;
        }
        protected override Boolean OnBackButtonPressed()
        {
            return true;
        }
        async void CreateAccount(object sender, EventArgs e)
        {

            //List<UserAccount> users;

            SignUpViewModel.user.cognom1 = cognom1Entry.Text;
            SignUpViewModel.user.cognom2 = cognom2Entry.Text;
            SignUpViewModel.user.nom = nomEntry.Text;
            SignUpViewModel.user.edat = edatEntry.Text;
            try
            {
                SignUpViewModel.user.genere = genereEntry.SelectedItem.ToString();
            }catch(Exception){}
            try
            {
                SignUpViewModel.user.professio = professioEntry.SelectedItem.ToString();
            }catch(Exception){}
            SignUpViewModel.user.anysExp = anysEntry.Text;

            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/register", string.Empty)); 
            HttpResponseMessage response = null;

            if (_SignUpViewModel.DataIsNotEmptyP2)
            {
                if (_SignUpViewModel.AgeFormatIsOk())
                {
                    if (_SignUpViewModel.ExpFormatIsOk())
                    {
                        var json = JsonConvert.SerializeObject(SignUpViewModel.user);
                        var content = new StringContent(json, Encoding.UTF8, "application/json");
                        response = await client.PostAsync(uri, content);

                        if (response.IsSuccessStatusCode) {
                            //UserAccount currentUser = new UserAccount { mUsername= SignUpViewModel.user.mUsername , nom= SignUpViewModel.user.nom , id=GetUserId()};
                            //Database.Instance.InsertObject(Tables.CURRENT_USER, currentUser);
                            CurrentUser.currentUser = new UserAccount();
                            CurrentUser.currentUser.id = GetUserId();
                            CurrentUser.currentUser.mUsername = SignUpViewModel.user.mUsername;
                            CurrentUser.currentUser.nom = SignUpViewModel.user.nom;
                            Application.Current.MainPage = new NavigationPage(new WelcomePage1());

                        }
                        //users = await Database.Instance.GetObject<List<UserAccount>>(Tables.REGISTERED_USERS);

                        //Si ja hi ha coses a la DB
                        /*if (users?.Any() ?? false)
                        {
                            if (!await _SignUpViewModel.UsernameAlreadyExists())
                            {

                                //Afegeixo el nou user a la llista que m'he baixat i la torno a guardar a la dB
                                users.Add(SignUpViewModel.user);
                                Database.Instance.InsertObject(Tables.REGISTERED_USERS, users);
                                Database.Instance.InsertObject(Tables.CURRENT_USER, SignUpViewModel.user.mUsername);
                                //Settings.IsLoggedIn = true;
                                Application.Current.MainPage = new NavigationPage(new WelcomePage1());

                        }
                            else
                            {
                                await DisplayAlert("Error", AppStrings.ErrorExistingUser, "OK");
                            }

                        }
                        else
                        {
                            bool error = await _SignUpViewModel.UsernameAlreadyExists();
                            if (!error)
                            {
                                users = new List<UserAccount>
                                {
                                    SignUpViewModel.user
                                };
                                Database.Instance.InsertObject(Tables.REGISTERED_USERS, users);
                                Database.Instance.InsertObject(Tables.CURRENT_USER, SignUpViewModel.user.mUsername);
                                //Settings.IsLoggedIn = true;
                                Application.Current.MainPage = new NavigationPage(new WelcomePage1());

                        }*/
                        else
                        {
                            await DisplayAlert("Error", AppStrings.ErrorExistingUser, "OK");
                        }



                    }
                    else
                    {
                        await DisplayAlert("Error", AppStrings.WrongYearsExpnput, "OK");
                    }
                }
                else
                {
                    await DisplayAlert("Error", AppStrings.WrongAgeInput, "OK");
                }                
             

                }else{

                    await DisplayAlert("Error", AppStrings.ErrorEmptyFields, "OK");
            }
        }

       long GetUserId()
        {

            HttpClient client = new HttpClient(new NativeMessageHandler());
            var uri = new Uri(string.Format(CommonUrl.Url + "/user", string.Empty));
            HttpResponseMessage response = null;

            var json = JsonConvert.SerializeObject(SignUpViewModel.user);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            response = Task.Run(() => client.PostAsync(uri, content)).Result;

            if (response.IsSuccessStatusCode)
            {
                var c = response.Content.ReadAsStringAsync().Result;
                var r = JsonConvert.DeserializeObject<UserAccount>(c);
                return r.id;
            }
            return 0;
        }

         void SignUpP2Cancel(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Login());
        }
    }
}
