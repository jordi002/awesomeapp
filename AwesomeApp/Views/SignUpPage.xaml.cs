﻿using System;
using System.Collections.Generic;
using AwesomeApp.Models;
using AwesomeApp.ViewModels;
using System.Linq;
using AwesomeApp.Views;
using System;
using Xamarin.Forms;
using Akavache;

namespace AwesomeApp
{
    public partial class SignUpPage : ContentPage
    {
        SignUpViewModel _SignUpViewModel;
        Boolean ButtonEnabled;
        public SignUpPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            

            _SignUpViewModel = new SignUpViewModel();
            BindingContext = _SignUpViewModel;

        }
        protected override Boolean OnBackButtonPressed()
        {
            Application.Current.MainPage = new NavigationPage(new Login());
            return true;
        }

        async void SignUpContinue(object sender, EventArgs e)
        {
            SignUpViewModel.user.mUsername = userEntry.Text;
            SignUpViewModel.user.password = passEntry.Text;
            SignUpViewModel.user.mEmail = emailEntry.Text;

            if (_SignUpViewModel.DataIsNotEmptyP1)
            {
                SignUpViewModel.user.mEmail = SignUpViewModel.user.mEmail.ToLower();

  
                        if(_SignUpViewModel.PasswordFormatIsOk()){
                            if(_SignUpViewModel.EmailFormatIsOk()){ 
    							await Navigation.PushAsync(new SignUpPage_Part2());
                            }else{
                            await DisplayAlert("Error", AppStrings.WrongEmailFormat, "OK");
                            }
                        }else{
                        await DisplayAlert("Error", AppStrings.WrongPasswordFormat, "OK");
                        }

            }else{            
                await DisplayAlert("Error", AppStrings.ErrorEmptyFields, "OK");
            }
        }

         void SignUpCancel(object sender, EventArgs e)
        {
            Application.Current.MainPage = new NavigationPage(new Login());
        }

        void OpenLink(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.udl.cat/ca/legal/"));
        }
        void OnCheckBoxCheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("********************    CHECKED");
            if (e.Value){
                _SignUpViewModel.ButtonEnabled = true;
            }
            else
            {
                _SignUpViewModel.ButtonEnabled = false;
            }
        }
    }
}
