﻿using AwesomeApp.Common;
using AwesomeApp.iOS;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;
using Xamarin.Forms;

[assembly: Dependency(typeof(Mail_IOS))]
namespace AwesomeApp.iOS
{
    public class Mail_IOS : IMail
    {
        public void SendMail(string username, string password, string email)
        {
            {
                var fromAddress = new MailAddress("jgm17tfg@gmail.com", "Dr.Care");
                var toAddress = new MailAddress(email, username);
                const string fromPassword = "provestfg";
                const string subject = "PASSWORD-RECOVERY";
                string body = "<center> <h1>"+ AppStrings.RecoveryBodyMailH1 + "</h1> <br> " +
                              AppStrings.RecoveryBodyMailString1 +
                              "<b>" + username + "</b> "+ AppStrings.RecoveryBodyMailString2 + " <br> " +
                              "<b> " + password + "</b> </center>";

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    Timeout = 10000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };

                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    message.IsBodyHtml = true;
                    smtp.Send(message);

                }
            }
        }
    }
}
